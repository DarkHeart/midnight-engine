#include <gtest/gtest.h>

#include "Line.hpp"

using namespace midnight;

TEST(Line, ConstructorPointVectorByConstReference)
{
	Point3F arbitraryPoint;
	Vector3F vector(1.0f, 1.0f, 1.0f);
	Line3F line(arbitraryPoint, vector);
	ASSERT_TRUE(arbitraryPoint == line.getPoint());
	ASSERT_TRUE(vector == line.getVector());
	Vector3F zero;
	ASSERT_THROW(Line3F(arbitraryPoint, zero), IllegalArgumentException);
}

TEST(Line, ConstructorPointPointByConstReference)
{
	Point3F p1, p2, p3(1, 1, 1);
	Line3F line(p1, p3);
	ASSERT_TRUE(line.getVector() == Vector3F(-1, -1, -1) || line.getVector() == Vector3F(1, 1, 1));
	ASSERT_THROW(Line3F(p1, p2), IllegalArgumentException);
}

TEST(Line, ConstructorPointVectorByRvalueReference)
{
	Point3F p1;
	Vector3F zero, vector(1.0f, 1.0f, 1.0f);
	ASSERT_THROW(Line3F(std::move(p1), std::move(zero)), IllegalArgumentException);
	ASSERT_NO_THROW(zero = *&zero);

	Line3F line(std::move(p1), std::move(vector));
	ASSERT_TRUE(Point3F(0, 0, 0) == line.getPoint());
	ASSERT_TRUE(Vector3F(1, 1, 1) == line.getVector());
}

TEST(Line, ConstructorPointPointByRvalueReference)
{
	Point3F p1, p2, p3(1, 1, 1);
	ASSERT_THROW(Line3F(std::move(p1), std::move(p2)), IllegalArgumentException);
	Line3F line(std::move(p1), std::move(p3));
	ASSERT_TRUE(line.getVector() == Vector3F(-1, -1, -1) || line.getVector() == Vector3F(1, 1, 1));
}

TEST(Line, CopyConstructor)
{
	Point3F p1(2, 2, 2);
	Point3F p2(p1);
	ASSERT_TRUE(p1 == p2);
}

TEST(Line, MoveConstructor)
{
	Point3F p1(2, 2, 2);
	Point3F p2(std::move(p1));
	ASSERT_TRUE(p2 == Point3F(2, 2, 2));
}

TEST(Line, CopyAssignment)
{
	Point3F p1(2, 2, 2);
	Point3F p2 = p1;
	ASSERT_TRUE(p1 == p2);
}

TEST(Line, MoveAssignment)
{
	Point3F p1(2, 2, 2);
	Point3F p2 = std::move(p1);
	ASSERT_TRUE(p2 == Point3F(2, 2 , 2));
}

TEST(Line, Intersection)
{
	Line3F l1(Point3F(), Vector3F(1, 1, 0));
	Line3F l2(Point3F(1, 2, 0), Vector3F(-1, 1, 0));
	Line3F l3(Point3F(1, 2, 1), Vector3F(-1, 44, 0));
	/*              __
	 			|    /|
	 			|\  /
				| \/
				| /\
________________ /__\______________________
				/    \
	 		   /	|     \
	 		  /	|      \
	 			|
	 
	 p1(0, 0, 0) p2(1, 1, 0), m = 1/1
	 p1(1, 2, 0) p2(0, 3, 0), m = -1/1
	 * 	 
	 */
	ASSERT_TRUE(l1.intersectsWith(l2));
	ASSERT_TRUE(l1.intersectsWith(l1));
	ASSERT_FALSE(l1.intersectsWith(l3));
}
