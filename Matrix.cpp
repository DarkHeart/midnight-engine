#include <gtest/gtest.h>

#include "Matrix.hpp"

using namespace midnight;

TEST(Matrix, DefaultConstructor)
{
	Matrix3x3F matrix;
	ASSERT_FLOAT_EQ(matrix(0, 0), 0);	ASSERT_FLOAT_EQ(matrix(1, 0), 0);	ASSERT_FLOAT_EQ(matrix(2, 0), 0);
	ASSERT_FLOAT_EQ(matrix(0, 1), 0);	ASSERT_FLOAT_EQ(matrix(1, 1), 0);	ASSERT_FLOAT_EQ(matrix(2, 1), 0);
	ASSERT_FLOAT_EQ(matrix(0, 2), 0);	ASSERT_FLOAT_EQ(matrix(1, 2), 0);	ASSERT_FLOAT_EQ(matrix(2, 2), 0);
}

TEST(Matrix, ArrayConstructorByCopy)
{
	std::array<float, 9> array = {{1, 2, 3, 4, 5, 6, 7, 8, 9}};
	Matrix3x3F matrix(array);
	ASSERT_FLOAT_EQ(matrix(0, 0), 1);	ASSERT_FLOAT_EQ(matrix(1, 0), 2);	ASSERT_FLOAT_EQ(matrix(2, 0), 3);
	ASSERT_FLOAT_EQ(matrix(0, 1), 4);	ASSERT_FLOAT_EQ(matrix(1, 1), 5);	ASSERT_FLOAT_EQ(matrix(2, 1), 6);
	ASSERT_FLOAT_EQ(matrix(0, 2), 7);	ASSERT_FLOAT_EQ(matrix(1, 2), 8);	ASSERT_FLOAT_EQ(matrix(2, 2), 9);
}

TEST(Matrix, ArrayConstructorByMove)
{
	std::array<float, 9> array = {{1, 2, 3, 4, 5, 6, 7, 8, 9}};
	Matrix3x3F matrix(std::move(array));
	ASSERT_FLOAT_EQ(matrix(0, 0), 1);	ASSERT_FLOAT_EQ(matrix(1, 0), 2);	ASSERT_FLOAT_EQ(matrix(2, 0), 3);
	ASSERT_FLOAT_EQ(matrix(0, 1), 4);	ASSERT_FLOAT_EQ(matrix(1, 1), 5);	ASSERT_FLOAT_EQ(matrix(2, 1), 6);
	ASSERT_FLOAT_EQ(matrix(0, 2), 7);	ASSERT_FLOAT_EQ(matrix(1, 2), 8);	ASSERT_FLOAT_EQ(matrix(2, 2), 9);
}

TEST(Matrix, ParameterPackConstructor)
{
	Matrix3x3F matrix(1, 2, 3, 
					  4, 5, 6, 
					  7, 8, 9);
	ASSERT_FLOAT_EQ(matrix(0, 0), 1);	ASSERT_FLOAT_EQ(matrix(1, 0), 2);	ASSERT_FLOAT_EQ(matrix(2, 0), 3);
	ASSERT_FLOAT_EQ(matrix(0, 1), 4);	ASSERT_FLOAT_EQ(matrix(1, 1), 5);	ASSERT_FLOAT_EQ(matrix(2, 1), 6);
	ASSERT_FLOAT_EQ(matrix(0, 2), 7);	ASSERT_FLOAT_EQ(matrix(1, 2), 8);	ASSERT_FLOAT_EQ(matrix(2, 2), 9);
}

TEST(Matrix, ZeroFactory)
{
	Matrix3x3F zero = Matrix3x3F::ZERO();
	ASSERT_FLOAT_EQ(zero(0, 0), 0);	ASSERT_FLOAT_EQ(zero(1, 0), 0);	ASSERT_FLOAT_EQ(zero(2, 0), 0);
	ASSERT_FLOAT_EQ(zero(0, 1), 0);	ASSERT_FLOAT_EQ(zero(1, 1), 0);	ASSERT_FLOAT_EQ(zero(2, 1), 0);
	ASSERT_FLOAT_EQ(zero(0, 2), 0);	ASSERT_FLOAT_EQ(zero(1, 2), 0);	ASSERT_FLOAT_EQ(zero(2, 2), 0);
}

TEST(Matrix, IdentityFactory)
{
	Matrix3x3F identity = Matrix3x3F::IDENTITY();
	ASSERT_FLOAT_EQ(identity(0, 0), 1);	ASSERT_FLOAT_EQ(identity(1, 0), 0);	ASSERT_FLOAT_EQ(identity(2, 0), 0);
	ASSERT_FLOAT_EQ(identity(0, 1), 0);	ASSERT_FLOAT_EQ(identity(1, 1), 1);	ASSERT_FLOAT_EQ(identity(2, 1), 0);
	ASSERT_FLOAT_EQ(identity(0, 2), 0);	ASSERT_FLOAT_EQ(identity(1, 2), 0);	ASSERT_FLOAT_EQ(identity(2, 2), 1);
}

TEST(Matrix, PerspectiveFactory_no_test_ready)
{
	// TODO: Figure out how to test this one!
	EXPECT_TRUE(false);
}

TEST(Matrix, EqualityOperator)
{
	ASSERT_TRUE(Matrix3x3F::ZERO() == (Matrix3x3F()));
}

TEST(Matrix, InequalityOperator)
{
	ASSERT_TRUE(Matrix3x3F::ZERO() != Matrix3x3F::IDENTITY());
}

TEST(Matrix, Negation)
{
	Matrix3x3F matrix;
	matrix.set(1, 2, 3, 
			   4, 5, 6, 
			   7, 8, 9);
	matrix = -matrix;
	
	ASSERT_FLOAT_EQ(matrix(0, 0), -1);	ASSERT_FLOAT_EQ(matrix(1, 0), -2);	ASSERT_FLOAT_EQ(matrix(2, 0), -3);
	ASSERT_FLOAT_EQ(matrix(0, 1), -4);	ASSERT_FLOAT_EQ(matrix(1, 1), -5);	ASSERT_FLOAT_EQ(matrix(2, 1), -6);
	ASSERT_FLOAT_EQ(matrix(0, 2), -7);	ASSERT_FLOAT_EQ(matrix(1, 2), -8);	ASSERT_FLOAT_EQ(matrix(2, 2), -9);
}

TEST(Matrix, Translation)
{
	Matrix3x3F matrix;
	Vector3F translation(1.0f, 1.0f, 1.0f);
	matrix.translate(translation);
	
	ASSERT_FLOAT_EQ(matrix(0, 0), 0);	ASSERT_FLOAT_EQ(matrix(1, 0), 0);	ASSERT_FLOAT_EQ(matrix(2, 0), 1);
	ASSERT_FLOAT_EQ(matrix(0, 1), 0);	ASSERT_FLOAT_EQ(matrix(1, 1), 0);	ASSERT_FLOAT_EQ(matrix(2, 1), 1);
	ASSERT_FLOAT_EQ(matrix(0, 2), 0);	ASSERT_FLOAT_EQ(matrix(1, 2), 0);	ASSERT_FLOAT_EQ(matrix(2, 2), 1);

	translation.set(-2.0f, -2.0f, -2.0f);
	matrix.translate(translation);
	
	ASSERT_FLOAT_EQ(matrix(0, 0), 0);	ASSERT_FLOAT_EQ(matrix(1, 0), 0);	ASSERT_FLOAT_EQ(matrix(2, 0), -1);
	ASSERT_FLOAT_EQ(matrix(0, 1), 0);	ASSERT_FLOAT_EQ(matrix(1, 1), 0);	ASSERT_FLOAT_EQ(matrix(2, 1), -1);
	ASSERT_FLOAT_EQ(matrix(0, 2), 0);	ASSERT_FLOAT_EQ(matrix(1, 2), 0);	ASSERT_FLOAT_EQ(matrix(2, 2), -1);
}

TEST(Matrix, Set)
{
	Matrix3x3F matrix;
	matrix.set(1, 2, 3, 
			   4, 5, 6, 
			   7, 8, 9);
	
	ASSERT_FLOAT_EQ(matrix(0, 0), 1);	ASSERT_FLOAT_EQ(matrix(1, 0), 2);	ASSERT_FLOAT_EQ(matrix(2, 0), 3);
	ASSERT_FLOAT_EQ(matrix(0, 1), 4);	ASSERT_FLOAT_EQ(matrix(1, 1), 5);	ASSERT_FLOAT_EQ(matrix(2, 1), 6);
	ASSERT_FLOAT_EQ(matrix(0, 2), 7);	ASSERT_FLOAT_EQ(matrix(1, 2), 8);	ASSERT_FLOAT_EQ(matrix(2, 2), 9);
}