#ifndef MESH_HPP
#define MESH_HPP

#include <cstdint>
#include <iostream>
#include <vector>

#include "Point.hpp"
#include "Vector.hpp"

#include "Material.hpp"

namespace midnight
{
    /// Forward Declaration for our friends
    template<typename T>
    class Mesh;
}

/**
 * Inserts the provided Mesh into the provided output stream
 * 
 * @param stream the output stream to write to
 * 
 * @param mesh the Mesh to insert
 * 
 * @return the provided output stream
 * 
 */
template<typename T>
std::ostream& operator<<(std::ostream& stream, const midnight::Mesh<T>& mesh);

/**
 * Inserts the provided Mesh into the provided output stream
 * 
 * @param stream the output stream to write to
 * 
 * @param mesh the Mesh to insert
 * 
 * @return the provided output stream
 * 
 * @note [optimization] this method should be preferred when the provided Mesh is not used by the calling method
 * 
 */
template<typename T>
std::ostream& operator<<(std::ostream& stream, midnight::Mesh<T>&& mesh);

/**
 * Extracts a Mesh from the provided input stream
 * 
 * @param stream the input stream to read from
 * 
 * @param mesh the Mesh to populate with the extracted data
 * 
 * @return the provided input stream
 * 
 */
template<typename T>
std::istream& operator>>(std::istream& stream, midnight::Mesh<T>& mesh);

namespace midnight
{

    /**
     * A general-purpose static 3D mesh implementation
     * 
     */
    template<typename T>
    class Mesh
    {
        friend std::ostream& operator<<<>(std::ostream&, const Mesh<T>&);
        friend std::ostream& operator<<<>(std::ostream&, Mesh<T>&&);
        friend std::istream& operator>><>(std::istream&, Mesh&);

        /// The position data of this Mesh
        std::vector<Point<T, 3 >> positions;

        /// The texture-coordinate data of this Mesh
        std::vector<Point<T, 2 >> texCoords;

        /// The normal data of this Mesh
        std::vector<Vector<T, 3 >> normals;

        /// The Material data of this Mesh
        std::vector<Material> materials;

        /**
         * A discrete rendering unit
         * 
         */
        struct Renderable
        {
            /// The index of the Material to use in this sub-mesh
            std::size_t materialIndex;
            
            /// The list of indices of this sub-mesh
            std::vector<std::size_t> indices;
        };
        
        /// The sub-meshes of this Mesh
        std::vector<Renderable> meshes;
        
      public:

        /**
         * Retrieves the (xyz) positions of this Mesh
         * 
         * @return the (xyz) positions of this Mesh
         * 
         */
        const std::vector<Point<T, 3>>& getPositions() const noexcept;

        /**
         * Retrieves the (uv) texture-coordinates of this Mesh
         * 
         * @return the (uv) texture-coordinates of this Mesh
         * 
         */
        const std::vector<Point<T, 2>>& getTexCoords() const noexcept;

        /**
         * Retrieves the (xyz) normals of this Mesh
         * 
         * @return the (xyz) normals of this Mesh
         * 
         */
        const std::vector<Vector<T, 3>>& getNormals() const noexcept;
    };

    template<typename T>
    inline const std::vector<Point<T, 3 >> &Mesh<T>::getPositions() const noexcept
    {
        return this->positions;
    }

    template<typename T>
    inline const std::vector<Point<T, 2 >> &Mesh<T>::getTexCoords() const noexcept
    {
        return this->texCoords;
    }

    template<typename T>
    inline const std::vector<Vector<T, 3 >> &Mesh<T>::getNormals() const noexcept
    {
        return this->normals;
    }

    template<typename T>
    inline std::ostream& operator<<(std::ostream& stream, const Mesh<T>& /*mesh*/)
    {
        // TODO: implement after finalizing class
        return stream;
    }

    template<typename T>
    inline std::ostream& operator<<(std::ostream& stream, Mesh<T>&& /*mesh*/)
    {
        // TODO: implement after finalizing class
        return stream;
    }

    template<typename T>
    inline std::istream& operator>>(std::istream& stream, Mesh<T>& /*mesh*/)
    {
        // TODO: implement after finalizing class
        return stream;
    }

    using Mesh3F = midnight::Mesh<float>;

}
#endif