#ifndef MESH_LOADER_HPP
#    define MESH_LOADER_HPP

#    include <fstream>
#    include <sstream>
#    include <vector>

#    include "Mesh.hpp"

namespace midnight
{
    namespace io
    {
        namespace spi
        {

            class MeshProvider
            {
              public:
                virtual bool isLoadableExtension(const std::string& extension) const noexcept = 0;
                virtual midnight::Mesh3F loadMesh(const std::string& file) = 0;
                virtual ~MeshProvider() = default;
            };

            std::vector<MeshProvider*> providers;
        }

        
        midnight::Mesh3F loadMeshFromDisk(const std::string& fileName)
        {
            std::string extension = fileName.substr(fileName.find_last_of('.'));
            for(spi::MeshProvider* provider : spi::providers)
            {
                if(provider->isLoadableExtension(extension))
                {
                    return provider->loadMesh(fileName);
                }
            }
            throw std::runtime_error("No known provider for " + extension + " format");
        }
    }
}

#endif
