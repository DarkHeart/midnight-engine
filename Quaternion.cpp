#include <gtest/gtest.h>

#include "Quaternion.hpp"

using namespace midnight;

TEST(Quaternion, LValueRefRadianConstructor)
{
	Radians<float> angle(1.0f);
	Vector3F axis(1.0f, 1.0f, 1.0f);
	
	ASSERT_NO_THROW((Quaternion<float>(axis, angle)));
	
	Quaternion<float> quaternion(axis, angle);
	ASSERT_FLOAT_EQ(quaternion.getAngle(), angle);
	ASSERT_TRUE(quaternion.getAxis() == axis);
}

TEST(Quaternion, RValueRefRadianConstructor)
{
	{
		Radians<float> angle(1.0f);
		Vector3F axis(1.0f, 1.0f, 1.0f);
		
		ASSERT_NO_THROW((Quaternion<float>(std::move(axis), std::move(angle))));
	}
	Radians<float> angle(1.0f);
	Vector3F axis(1.0f, 1.0f, 1.0f);
	Quaternion<float> quaternion(axis, angle);
	ASSERT_FLOAT_EQ(quaternion.getAngle(), angle);
	ASSERT_TRUE(quaternion.getAxis() == axis);
}

TEST(Quaternion, LValueRefDegreeConstructor)
{
	Degrees<float> angle(1.0f);
	Vector3F axis(1.0f, 1.0f, 1.0f);
	ASSERT_NO_THROW((Quaternion<float>(axis, angle)));
	
	Quaternion<float> quaternion(axis, angle);
	ASSERT_FLOAT_EQ(quaternion.getAngle(), toRadians(angle));
	ASSERT_TRUE(quaternion.getAxis() == axis);
}

TEST(Quaternion, RValueRefDegreeConstructor)
{
	{
		Degrees<float> angle(45.0f);
		Vector3F axis(1.0f, 1.0f, 1.0f);

		ASSERT_NO_THROW((Quaternion<float>(std::move(axis), std::move(angle))));
	}
	Degrees<float> angle(1.0f);
	Vector3F axis(1.0f, 1.0f, 1.0f);
	Quaternion<float> quaternion(std::move(axis), std::move(angle));
	ASSERT_FLOAT_EQ(quaternion.getAngle(), toRadians(angle));
	ASSERT_TRUE(quaternion.getAxis() == axis);
}

TEST(Quaternion, Accessors)
{
	Radians<float> angle(1.0f);
	Vector3F axis(1.0f, 1.0f, 1.0f);

	Quaternion<float> quaternion(axis, angle);
	ASSERT_FLOAT_EQ(quaternion.getAngle(), angle);
	ASSERT_TRUE(quaternion.getAxis() == axis);
}

TEST(Quaternion, Normalization)
{
	Radians<float> angle(1.0f);
	Vector3F axis(87.0f, 92.0f, -111.0f);

	Quaternion<float> quaternion(axis, angle);
	quaternion.normalize();
	ASSERT_FLOAT_EQ(quaternion.getAxis().length(), 1.0f);
}

TEST(Quaternion, Equality)
{
	Radians<float> angle0(1.0f),
				   angle1(1.0f),
				   angle2(0.5f);
	
	Vector3F axis0(87.0f, 92.0f, -111.0f), 
			 axis1(87.0f, 92.0f, -111.0f), 
			 axis2(87.0f, 92.0f, -113.0f);
	
	Quaternion<float> q0(axis0, angle0),
			   q1(axis1, angle1),
			   q2(axis2, angle2);
	
	ASSERT_TRUE(q0 == q1 && q1 == q0);
	ASSERT_FALSE(q0 == q2 || q1 == q2 || q2 == q1 || q2 == q0);
}

TEST(Quaternion, Inequality)
{
	Radians<float> angle0(1.0f),
				   angle1(1.0f),
				   angle2(0.5f);
	
	Vector3F axis0(87.0f, 92.0f, -111.0f), 
			 axis1(87.0f, 92.0f, -111.0f), 
			 axis2(87.0f, 92.0f, -113.0f);
	
	Quaternion<float> q0(axis0, angle0),
			   q1(axis1, angle1),
			   q2(axis2, angle2);
	
	ASSERT_TRUE(q0 != q2 && q1 != q2 && q2 != q0 && q2 != q1);
	ASSERT_TRUE(q0 != q1 && q1 != q0);
}

TEST(Quaternion, Multiplication)
{
	Radians<float> angle0(1.0f),
				   angle1(1.0f),
				   angle2(0.5f);
	
	Vector3F axis0(87.0f, 92.0f, -111.0f), 
			 axis1(87.0f, 92.0f, -111.0f), 
			 axis2(87.0f, 92.0f, -113.0f);
	
	Quaternion<float> q0(axis0, angle0),
			   q1(axis1, angle1),
			   q2(axis2, angle2);
	
	q0 *= q1;
	q0 *= q2;
	
	Quaternion<float> q3 = q0 * q1;
}

TEST(Quaternion, ExplicitConversions)
{
	Radians<float> angle0(1.0f);
	
	Vector3F axis0(87.0f, 92.0f, -111.0f);
	
	Quaternion<float> q0(axis0, angle0);
	
	Matrix4x4F matrix = (Matrix4x4F)q0;
	std::cout << matrix << std::endl;
}