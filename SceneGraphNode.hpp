#ifndef SCENEGRAPH_NODE_HPP
#define SCENEGRAPH_NODE_HPP

#include <vector>
#include <memory>

#include "Camera.hpp"

namespace midnight
{

class SceneGraphNode
{
    std::vector<std::shared_ptr<SceneGraphNode>> children;

  protected:
    
    const std::vector<std::shared_ptr<SceneGraphNode>>& getChildren() const
    {
        return children;
    }
    
  public:

    SceneGraphNode()
    {
        
    }

    virtual void render(const Camera& camera)
    {
        for(const auto& child : children)
        {
            child->render(camera);
        }
    }
        
    virtual void add(std::shared_ptr<SceneGraphNode> child)
    {
        children.push_back(child);
    }
    
    virtual bool isPickable()
    {
        return false;
    }
    
    virtual ~SceneGraphNode()
    {
        
    }
};

}

#endif