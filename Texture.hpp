#ifndef TEXTURE_HPP
#    define TEXTURE_HPP

#    include "Platform.hpp"

#    include "lodepng.h"

namespace midnight
{

std::tuple<GLsizei, GLsizei, std::vector<unsigned char >> loadPNG(const char* filename)
{
    std::vector<unsigned char> image; //the raw pixels
    unsigned width, height;


    //decode
    unsigned error = lodepng::decode(image, width, height, filename);

    //if there's an error, display it
    if(error) std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;
    return std::make_tuple(static_cast<GLsizei>(width), static_cast<GLsizei>(height), image);
}

class Texture
{
    std::vector<unsigned char> data;

  public:
    GLuint handle;

    Texture(const char* fileName)
    {
        glGenTextures(1, &handle);
        glBindTexture(GL_TEXTURE_2D, handle);
        auto data = loadPNG(fileName);
        glTexImage2D(GL_TEXTURE_2D, 0, 4, std::get<0>(data), std::get<1>(data), 0, GL_RGBA, GL_UNSIGNED_BYTE, &std::get<2>(data)[0]);
    }

    Texture(std::size_t width, std::size_t height, const std::vector<unsigned char>& data)
    {
        glGenTextures(1, &handle);
        glBindTexture(GL_TEXTURE_2D, handle);
        glTexImage2D(GL_TEXTURE_2D, 0, 4, static_cast<GLsizei>(width), static_cast<GLsizei>(height), 0, GL_RGBA, GL_UNSIGNED_BYTE, &data[0]);
    }
    
    void bind()
    {
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, handle);
    }

    void unbind()
    {
        glBindTexture(GL_TEXTURE_2D, 0);
        glDisable(GL_TEXTURE_2D);
    }
};

}

#endif