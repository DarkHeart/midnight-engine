/**
 * 
 * This is an internal file and should not be included directly by users.
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * This file contains the data structures that represent the Wavefront OBJ file format.
 * 
 * 
 * It should be explicitly noted that this is only an extremely limited 
 * subset of the proprietary OBJ format owned by WaveFront technologies.
 * 
 * Among the unsupported features are:
 * 1) Modular calling support
 * 2) Parameter-space vertices, curves, degrees, basis matrices, and steps
 * 3) Points, lines, curves, 2D curves, and surface elements
 * 4) Free-form curve / surface body statements
 * 5) Inter-connectivity of free-form surfaces
 * 6) Display / Render attributes _EXCEPT_ material libraries and names
 * 
 * @version 1.0, 3.22.2014
 * 
 * @since 1.0
 * 
 * @author Luke A. Leber
 * 
 */

#ifndef OBJ_FORMAT_HPP
#    define OBJ_FORMAT_HPP

#    include <cstdint>          /// std::size_t
#    include <vector>           /// std::vector
#    include <string>           /// std::string

#    include "obj_material.hpp" /// midnight::obj_detail::Material
#    include "Point.hpp"        /// midnight::Point
#    include "Vector.hpp"       /// midnight::Vector

namespace obj_detail
{

    /**
     * A face is represented by a set of data consisting of:
     *  <ul>
     *      <li>vertex positions</li>
     *      <li>vertex normals</li>
     *      <li>vertex texture coordinates</li>
     * </ul>
     *  
     * This data is provided through offsets into a monolithic lookup-table specific to each mesh.
     * 
     */
    class Face
    {
        /// A vector containing offsets to the face position data
        std::vector<std::size_t> vertexIndices;

        /// A vector containing the offsets to the face normal data
        std::vector<std::size_t> normalIndices;

        /// A vector containing the offsets to the face texture coordinate data
        std::vector<std::size_t> texCoordIndices;

      public:

        /**
         * Constructs a Face with the provided data set
         * 
         * @param vertexIndices a vector containing offsets to the face position data
         * 
         * @param normalIndices a vector containing the offsets to the face normal data
         * 
         * @param texCoordIndices a vector containing the offsets to the face texture coordinate data
         * 
         * @note Prefer this constructor if the caller has further use for the data set
         * 
         */
        Face(const std::vector<std::size_t>& vertexIndices,
                const std::vector<std::size_t>& normalIndices,
                const std::vector<std::size_t>& texCoordIndices)
        :
        vertexIndices(vertexIndices),
        normalIndices(normalIndices),
        texCoordIndices(texCoordIndices)
        {

        }

        /**
         * Constructs a Face with the provided data set
         * 
         * @param vertexIndices a vector containing offsets to the face position data
         * 
         * @param normalIndices a vector containing the offsets to the face normal data
         * 
         * @param texCoordIndices a vector containing the offsets to the face texture coordinate data
         * 
         * @note Prefer this constructor if the caller has no further use for the data set
         * 
         */
        Face(std::vector<std::size_t>&& vertexIndices,
                std::vector<std::size_t>&& normalIndices,
                std::vector<std::size_t>&& texCoordIndices) /*noexcept*/ // TODO: check
        :
        vertexIndices(std::move(vertexIndices)),
        normalIndices(std::move(normalIndices)),
        texCoordIndices(std::move(texCoordIndices))
        {

        }

        /**
         * Retrieves the vertex position data of this Face
         * 
         * @return the vertex position data of this Face
         * 
         */
        const std::vector<std::size_t>& getVertices() const noexcept
        {
            return vertexIndices;
        }

        /**
         * Retrieves the vertex normal data of this Face
         * 
         * @return the vertex normal data of this Face
         * 
         */
        const std::vector<std::size_t>& getNormals() const noexcept
        {
            return normalIndices;
        }

        /**
         * Retrieves the vertex texture coordinate data of this Face
         * 
         * @return the vertex texture coordinate data of this Face
         * 
         */
        const std::vector<std::size_t>& getTexCoords() const noexcept
        {
            return texCoordIndices;
        }
    };

    /**
     * A Group is a group of Faces that use the same Material.
     * 
     */
    class Group
    {
        /// The index of the Material used by this Group
        std::size_t materialIndex;

        /// A vector of offsets to Faces that make up this Group
        std::vector<std::size_t> faceIndices;

      public:

        /**
         * Constructs a Group with the provided Material index and vector of Faces
         * 
         * @param materialIndex the index of the Material used by this Group
         * 
         * @param faceIndices a vector of offsets to Faces that make up this Group
         * 
         * @note Prefer this constructor if the caller has further use for the Face set
         * 
         */
        Group(std::size_t materialIndex, const std::vector<std::size_t>& faceIndices) :
        materialIndex(materialIndex),
        faceIndices(faceIndices)
        {

        }

        /**
         * Constructs a Group with the provided Material index and vector of Faces
         * 
         * @param materialIndex the index of the Material used by this Group
         * 
         * @param faceIndices a vector of offsets to Faces that make up this Group
         * 
         * @note Prefer this constructor if the caller has no further use for the Face set
         * 
         */
        Group(std::size_t materialIndex, std::vector<std::size_t>&& faceIndices) :
        materialIndex(materialIndex),
        faceIndices(std::move(faceIndices))
        {

        }

        /**
         * Retrieves the Material index used by this Group
         * 
         * @return the Material index used by this Group
         */
        std::size_t getMaterialIndex() const noexcept
        {
            return materialIndex;
        }

        /**
         * Retrieves the vector of Faces that make up this Group
         * 
         * @return the vector of Faces that make up this Group
         * 
         */
        const std::vector<std::size_t>& getFaceIndices() const noexcept
        {
            return faceIndices;
        }
    };

    /**
     * A Mesh is the representation of an OBJ model.
     * 
     * Meshes contain monolithic vectors for the following data:
     * <ul>
     *  <li>Materials</li>
     *  <li>Vertex Positions</li>
     *  <li>Vertex Normals</li>
     *  <li>Vertex Texture Coordinates</li>
     *  <li>Faces</li>
     *  <li>Groups</li>
     * </ul>
     * 
     */
    class Mesh
    {
        /// A vector of Materials used in this Mesh
        std::vector<Material> materials;

        /// A vector of 3D coordinates used in this Mesh
        std::vector<midnight::Point3F> positions;

        /// A vector of 2D texture coordinates used in this Mesh
        std::vector<midnight::Point2F> texCoords;

        /// A vector of 3D normal vectors used in this Mesh
        std::vector<midnight::Vector3F> normals;

        /// A vector of Faces used in this Mesh
        std::vector<Face> faces;

        /// A vector of Groups used in this Mesh
        std::vector<Group> groups;

      public:

        /**
         * Constructs a Mesh with the provided data sets
         * 
         * @param materials the vector of Materials used in this Mesh
         * 
         * @param positions the vector of 3D coordinates used in this Mesh
         * 
         * @param texCoords the vector of 2D texture coordinates used in this Mesh
         * 
         * @param normals the vector of 3D normal vectors used in this Mesh
         * 
         * @param faces the vector of Faces used in this Mesh
         * 
         * @param groups the vector of Groups used in this Mesh
         * 
         * @note Prefer this constructor if the caller has further use for the data sets
         * 
         */
        Mesh(const std::vector<Material>& materials,
                const std::vector<midnight::Point3F>& positions,
                const std::vector<midnight::Point2F>& texCoords,
                const std::vector<midnight::Vector3F>& normals,
                const std::vector<Face>& faces,
                const std::vector<Group>& groups) :
        materials(materials),
        positions(positions),
        texCoords(texCoords),
        normals(normals),
        faces(faces),
        groups(groups)
        {

        }

        /**
         * Constructs a Mesh with the provided data sets
         * 
         * @param materials the vector of Materials used in this Mesh
         * 
         * @param positions the vector of 3D coordinates used in this Mesh
         * 
         * @param texCoords the vector of 2D texture coordinates used in this Mesh
         * 
         * @param normals the vector of 3D normal vectors used in this Mesh
         * 
         * @param faces the vector of Faces used in this Mesh
         * 
         * @param groups the vector of Groups used in this Mesh
         * 
         * @note Prefer this constructor if the caller has no further use for the data sets
         * 
         */
        Mesh(std::vector<Material>&& materials,
                std::vector<midnight::Point3F>&& positions,
                std::vector<midnight::Point2F>&& texCoords,
                std::vector<midnight::Vector3F>&& normals,
                std::vector<Face>&& faces,
                std::vector<Group>&& groups) :
        materials(std::move(materials)),
        positions(std::move(positions)),
        texCoords(std::move(texCoords)),
        normals(std::move(normals)),
        faces(std::move(faces)),
        groups(std::move(groups))
        {

        }

        /**
         * Retrieves the Materials used in this Mesh
         * 
         * @return the Materials used in this Mesh
         * 
         */
        const std::vector<Material>& getMaterialsBuffer() const noexcept
        {
            return materials;
        }

        /**
         * Retrieves the 3D coordinates used in this Mesh
         * 
         * @return the 3D coordinates used in this Mesh
         * 
         */
        const std::vector<midnight::Point3F>& getPositionBuffer() const noexcept
        {
            return positions;
        }

        /**
         * Retrieves the 2D texture coordinates used in this Mesh
         * 
         * @return the 2D texture coordinates used in this Mesh
         * 
         */
        const std::vector<midnight::Point2F>& getTexCoordBuffer() const noexcept
        {
            return texCoords;
        }

        /**
         * Retrieves the 3D normal vectors used in this Mesh
         * 
         * @return the 3D normal vectors used in this Mesh
         * 
         */
        const std::vector<midnight::Vector3F>& getNormalBuffer() const noexcept
        {
            return normals;
        }

        /**
         * Retrieves the Faces used in this Mesh
         * 
         * @return the Faces used in this Mesh
         * 
         */
        const std::vector<Face>& getFaceBuffer() const noexcept
        {
            return faces;
        }

        /**
         * Retrieves the Groups used in this Mesh
         * 
         * @return the Groups used in this Mesh
         * 
         */
        const std::vector<Group>& getGroupBuffer() const noexcept
        {
            return groups;
        }
    };
}

#endif
