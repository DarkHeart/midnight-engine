#ifndef OBJ_LOADER_HPP
#define OBJ_LOADER_HPP

#include <fstream>
#include <sstream>

#include "obj_parser.hpp"

namespace midnight
{
    namespace io
    {
        namespace spi
        {
            class OBJMeshProvider : public MeshProvider
            {
              public:
                
                bool isLoadableExtension(const std::string& extension) const noexcept
                {
                    return extension == ".obj";
                }
                
                midnight::Mesh3F loadMesh(const std::string& file)
                {
                    std::ifstream in(file.c_str());
                    std::stringstream ss;
                    ss << in.rdbuf();
                    std::string src(ss.str());
                    obj_detail::Mesh mesh = obj_detail::readOBJ(src.begin(), src.end());
                    
                    for(auto group : mesh.getGroupBuffer())
                    {
                        for(auto index : group.getFaceIndices())
                        {
                            auto face = mesh.getFaceBuffer()[index];
                        }
                    }
                    throw;
                }
            };
        }
    }
}
#endif
