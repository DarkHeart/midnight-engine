/**
 * 
 * This is an internal file and should not be included directly by users.
 * 
 * TODO: Finish Documentation
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * This file contains the data structures that represent the Wavefront MTL ASCII file format.
 * 
 * @version 1.0, 3.22.2014
 * 
 * @since 1.0
 * 
 * @author Luke A. Leber
 * 
 */

#ifndef OBJ_MATERIAL_HPP
#    define OBJ_MATERIAL_HPP

#    include <map>                  /// std::map
#    include <string>               /// std::string
#    include <tuple>                /// std::tuple

#    include <boost/variant.hpp>    /// boost::variant

#    include "obj_material_tokens.hpp"

namespace obj_detail
{

    /**
     * Texture map statements modify the material parameters of a surface by 
     * associating an image or texture file with material parameters that can 
     * be mapped.  By modifying existing parameters instead of replacing them, 
     * texture maps provide great flexibility in changing the appearance of an 
     * object's surface.
     *  
     *  Image files and texture files can be used interchangeably.  If you use 
     * an image file, that file is converted to a texture in memory and is 
     * discarded after rendering.
     *  
     *  Tip Using images instead of textures saves disk space and setup time, 
     * however, it introduces a small computational cost at the beginning of a 
     * render.
     *  
     *  The material parameters that can be modified by a texture map are:
     *  
     *  - Ka (color)
     *  - Kd (color)
     *  - Ks (color)
     *  - Ns (scalar)
     *  - d (scalar)
     *  
     *  In addition to the material parameters, the surface normal can be 
     * modified.
     *  
     *  
     *  Image file types
     *  
     *  You can link any image file type that is currently supported.  
     * Supported image file types are listed in the chapter "About Image" in 
     * the "Advanced Visualizer User's Guide".  You can also use the "im_info -
     * a" command to list Image file types, among other things.
     *  
     *  
     *  Texture file types
     *  
     *  The texture file types you can use are:
     *  
     *  - mip-mapped texture files (.mpc, .mps, .mpb)
     *  - compiled procedural texture files (.cxc, .cxs, .cxb)
     *  
     *  
     *  Mip-mapped texture files
     *  
     *  Mip-mapped texture files are created from images using the Create 
     * Textures panel in the Director or the "texture2D" program.  There are 
     * three types of texture files:
     *  
     *  - color texture files (.mpc)
     *  - scalar texture files (.mps)
     *  - bump texture files (.mpb)
     *  
     *  Color textures.  Color texture files are designated by an extension of 
     * ".mpc" in the filename, such as "chrome.mpc".  Color textures modify the 
     * material color as follows:
     *  
     *  - Ka - material ambient is multiplied by the texture value
     *  - Kd - material diffuse is multiplied by the texture value
     *  - Ks - material specular is multiplied by the texture value
     * 
     *  Scalar textures.  Scalar texture files are designated by an extension 
     * of ".mps" in the filename, such as "wisp.mps".  Scalar textures modify 
     * the material scalar values as follows:
     *  
     *  - Ns - material specular exponent is multiplied by the texture value
     *  - d - material dissolve is multiplied by the texture value
     *  - decal - uses a scalar value to deform the surface of an object to 
     * create surface roughness
     *  
     *  Bump textures.  Bump texture files are designated by an extension of 
     * ".mpb" in the filename, such as "sand.mpb".  Bump textures modify 
     * surface normals.  The image used for a bump texture represents the 
     * topology or height of the surface relative to the average surface.  Dark 
     * areas are depressions and light areas are high points.  The effect is 
     * like embossing the surface with the texture.
     *  
     *  
     *  Procedural texture files
     *  
     *  Procedural texture files use mathematical formulas to calculate sample 
     * values of the texture.  The procedural texture file is compiled, stored, 
     * and accessed by the Image program when rendering.  for more information 
     * see chapter 9, "Procedural Texture Files (.cxc, .cxb. and .cxs)".
     * 
     */
    class TextureMap
    {
        /// For setting options (implementation private)
        friend class Material;

        /// "union-like" type for holding all possible option data types
        typedef boost::variant<std::string, bool, uint32_t, float, char, std::tuple<float, float>, std::tuple<float, float, float >> TextureMapOption;

        /// Mutable to work around boost::get in a const method
        mutable std::map<std::string, TextureMapOption> options;

        /// The name of the file this texture map refers to
        const std::string fileName;

        template<typename T>
        const T& getOption(const std::string& key) const
        {
            auto iter = options.find(key);
            if(iter == options.end())
            {
                throw std::runtime_error("option " + key + " is not set");
            }
            return boost::get<T>(iter->second);
        }

        void setOption(const std::string& key, const TextureMapOption& option)
        {
            options.insert(std::make_pair(key, option));
        }

      public:

        explicit TextureMap(const std::string& fileName = "") :
        fileName(fileName)
        {
            // blendu defaults to 'ON'
            options.insert(std::make_pair(BLEND_U_OPTION_TOKEN, true));
            // blendv defaults to 'ON'
            options.insert(std::make_pair(BLEND_V_OPTION_TOKEN, true));
            // clamp defaults to 'OFF'
            options.insert(std::make_pair(CLAMP_OPTION_TOKEN, false));
        }

        bool blendU() const
        {
            return getOption<bool>(BLEND_U_OPTION_TOKEN);
        }

        bool blendV() const
        {
            return getOption<bool>(BLEND_V_OPTION_TOKEN);
        }

        float bumpMultiplier() const
        {
            return getOption<float>(BUMP_MULTIPLIER_OPTION_TOKEN);
        }

        float boost() const
        {
            return getOption<float>(BOOST_OPTION_TOKEN);
        }

        bool colorCorrection() const
        {
            return getOption<bool>(COLOR_CORRECTION_OPTION_TOKEN);
        }

        bool clamp() const
        {
            return getOption<bool>(CLAMP_OPTION_TOKEN);
        }

        char imfchan() const
        {
            return getOption<char>(CHANNEL_OPTION_TOKEN);
        }

        const std::tuple<float, float>& mm() const
        {
            return getOption < std::tuple<float, float >> (MODIFY_MAP_OPTION_TOKEN);
        }

        const std::tuple<float, float, float>& o() const
        {
            return getOption < std::tuple<float, float, float >> (OFFSET_OPTION_TOKEN);
        }

        const std::tuple<float, float, float>& s() const
        {
            return getOption < std::tuple<float, float, float >> (SCALE_OPTION_TOKEN);
        }

        const std::tuple<float, float, float>& t() const
        {
            return getOption < std::tuple<float, float, float >> (TURBULENCE_OPTION_TOKEN);
        }

        uint32_t texres() const
        {
            return getOption<uint32_t>(RESOLUTION_OPTION_TOKEN);
        }

        const std::string& type() const
        {
            return getOption<std::string>(TYPE_OPTION_TOKEN);
        }
    };

    /**
     * An enumeration of the color spaces supported by the format
     * 
     */
    enum class ColorSpace
    {
        RGB, /* < (http://en.wikipedia.org/wiki/RGB_color_space) */
        CIEXYZ /* < (http://en.wikipedia.org/wiki/CIE_1931_color_space) */
    };

    /**
     * A variant typedef that "unions" different ways of expressing reflectivity
     * 
     */
    typedef boost::variant<
    std::tuple<std::array<float, 3>, ColorSpace>,
    std::tuple<std::string, float>
    > Reflectivity;

    /**
     * A specialized visitor class to extend in order to access 
     * the values contained in the Reflectivity variants.
     * 
     */
    class ReflectivityVisitor : public boost::static_visitor<>
    {
      public:

        /**
         * Internally invoked for the 'rgb' / 'xyz' variant
         * 
         * @param rgb_xyz a tuple containing the values and ColorSpace
         * 
         */
        void operator()(const std::tuple<std::array<float, 3>,
                ColorSpace>& rgb_xyz)
        {
            this->operator()(std::get<0>(rgb_xyz)[0],
                    std::get<0>(rgb_xyz)[1],
                    std::get<0>(rgb_xyz)[2],
                    std::get<1>(rgb_xyz));
        }

        /**
         * Internally invoked for the 'spectral curve' variant
         * 
         * @param file_factor a tuple containing the file and multiplier
         * 
         */
        void operator()(const std::tuple<std::string, float> file_factor)
        {
            this->operator()(std::get<0>(file_factor),
                    std::get<1>(file_factor));
        }

        /**
         * Overridden by visitors, this method handles visits for the 'rgb / 'xyz' variant
         * 
         * @param r the red (or 'x') component
         * 
         * @param g the green (or 'y') component
         * 
         * @param b the blue (or 'z') component
         * 
         * @param colorSpace the ColorSpace that is used
         * 
         */
        virtual void operator()(float r,
                float g,
                float b,
                ColorSpace colorSpace) = 0;

        /**
         * Overridden by visitors, this method handles visits for the 'spectral curve' variant
         * 
         * @param fileName the name of the 'spectral curve' file
         * 
         * @param factor the factor of multiplication
         * 
         */
        virtual void operator()(const std::string& fileName, float factor) = 0;

        /**
         * Default virtual destructor
         * 
         */
        virtual ~ReflectivityVisitor() = default;
    };

    /**
     * TODO: better documentation -- can't be bothered to fully read the MTL spec right now.
     */
    class Material
    {
        /// The name of this material
        std::string name;

        /// The ambient reflectivity of this material
        Reflectivity Ka;

        /// The diffuse reflectivity of this material
        Reflectivity Kd;

        /// The spectral reflectivity of this material
        Reflectivity Ks;

        /// The illumination model of this material
        uint8_t illum;

        /// The dissolve factor of this material
        float d;

        /// The specular exponent of this material
        float Ns;

        /// The sharpness of this material
        float sharpness;

        /// The optical density of this material
        float Ni;

        /// The texture map used for ambience
        TextureMap map_Ka;

        /// The texture map used for diffusion
        TextureMap map_Kd;

        /// The texture map used for specularity
        TextureMap map_Ks;

        /// The scalar texture map linked to the specular exponent
        TextureMap map_Ns;

        /// The texture map used for dissolve
        TextureMap map_d;

        /// Is anti-aliasing enabled?
        bool map_aat;

        ///
        TextureMap decal;
        TextureMap bump;
        TextureMap refl;

      public:

        const std::string& getName() const
        {
            return this->name;
        }

        Material& setName(const std::string& name)
        {
            this->name = name;
            return *this;
        }

        const Reflectivity& getAmbientReflectivity() const
        {
            return this->Ka;
        }

        Material& setAmbientReflectivity(const Reflectivity& reflectivity)
        {
            this->Ka = reflectivity;
            return *this;
        }

        const Reflectivity& getDiffuseReflectivity() const
        {
            return this->Kd;
        }

        Material& setDiffuseReflectivity(const Reflectivity& reflectivity)
        {
            this->Kd = reflectivity;
            return *this;
        }

        const Reflectivity& getSpectralReflectivity() const
        {
            return this->Ks;
        }

        Material& setSpectralReflectivity(const Reflectivity& reflectivity)
        {
            this->Ks = reflectivity;
            return *this;
        }

        uint8_t getIlluminationModel() const
        {
            return this->illum;
        }

        Material& setIlluminationModel(uint8_t illuminationModel)
        {
            this->illum = illuminationModel;
            return *this;
        }

        float getDissolve() const
        {
            return this->d;
        }

        Material& setDissolve(float dissolve)
        {
            this->d = dissolve;
            return *this;
        }

        float getSpecularExponent() const
        {
            return this->Ns;
        }

        Material& setSpecularExponent(float exponent)
        {
            this->Ns = exponent;
            return *this;
        }

        float getSharpness() const
        {
            return this->sharpness;
        }

        Material& setSharpness(float sharpness)
        {
            this->sharpness = sharpness;
            return *this;
        }

        float getOpticalDensity() const
        {
            return this->Ni;
        }

        Material& setOpticalDensity(float density)
        {
            this->Ni = density;
            return *this;
        }

        const TextureMap& getAmbienceTextureMap() const
        {
            return this->map_Ka;
        }

        TextureMap& getAmbienceTextureMap()
        {
            return this->map_Ka;
        }

        const TextureMap& getDiffusionTextureMap() const
        {
            return this->map_Kd;
        }

        TextureMap& getDiffusionTextureMap()
        {
            return this->map_Kd;
        }

        const TextureMap& getSpecularityTextureMap() const
        {
            return this->map_Ks;
        }

        TextureMap& getSpecularityTextureMap()
        {
            return this->map_Ks;
        }

        const TextureMap& getSpecularityScalarTextureMap() const
        {
            return this->map_Ns;
        }

        TextureMap& getSpecularityScalarTextureMap()
        {
            return this->map_Ns;
        }

        const TextureMap& getDissolveTextureMap() const
        {
            return this->map_d;
        }

        TextureMap& getDissolveTextureMap()
        {
            return this->map_d;
        }

        bool isAntialised() const
        {
            return this->map_aat;
        }

        Material& setAntialiased(bool antialiased)
        {
            this->map_aat = antialiased;
            return *this;
        }

        const TextureMap& getDecalTextureMap() const
        {
            return this->decal;
        }

        TextureMap& getDecalTextureMap()
        {
            return this->decal;
        }

        const TextureMap& getBumpTextureMap() const
        {
            return this->bump;
        }

        TextureMap& getBumpTextureMap()
        {
            return this->bump;
        }

        const TextureMap& getReflectionTextureMap() const
        {
            return this->refl;
        }

        TextureMap& getReflectionTextureMap()
        {
            return this->refl;
        }

        Material()
        {

        }

    };
}
#endif