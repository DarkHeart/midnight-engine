#ifndef OBJ_MATERIAL_PARSER_HPP
#    define OBJ_MATERIAL_PARSER_HPP

#    include "obj_parser_commons.hpp"

namespace obj_detail
{

    inline std::string getName(std::string::iterator& begin, std::string::iterator end)
    {
        // Advance iterator to the first occurrence of NAME_STATEMENT_TOKEN or end
        while(begin != end && nextToken(begin, end) != NAME_STATEMENT_TOKEN);

        // Check to see if we've found the name token or not
        if(begin == end)
        {
            throw std::runtime_error("no name declaration found");
        }

        // Skip any whitespace between NAME_STATEMENT_TOKEN and the name token
        skipWhitespace(begin, end);

        // Grab the rest of the line as the name token
        std::string name = restOfLine(begin, end);

        // Validate the name per the specification
        if(name.find(' ') != std::string::npos || name.find('\t') != std::string::npos)
        {
            throw std::runtime_error(std::string("invalid characters found: ") + NAME_STATEMENT_TOKEN + name);
        }
        return name;
    }

    /**
     * Constructs an obj_material from the string formed by the provided iterator range
     * 
     * @param begin the iterator to begin with
     * 
     * @param end the iterator to end with
     * 
explicit obj_material(typename std::string::iterator begin, typename std::string::iterator end) : 
    name(obj_detail::getName(begin, end))
{
    while(begin != end)
    {
        using namespace obj_detail;
        switch(hash(nextToken(begin, end).c_str()))
        {
            case hash(AMBIENT_LIGHT_TOKEN):
            {
                std::string token = nextToken(begin, end);
                std::string line = restOfLine(begin, end);
                //std::string::iterator begin = line.begin();
            //	std::string::iterator end = line.end();
                if(token == "spectral")
                {
                //	this->Ka = std::make_tuple(nextToken(begin, end), nextFloat(begin, end));
                }
                else if(token == "xyz")
                {
                //	float _x = nextFloat(begin, end);
                    //float _y = nextFloat(begin, end, _x);
                //	float _z = nextFloat(begin, end, _y);
                   // this->Ka = std::make_tuple(_x, _y, _z);
//					float x = std::stof(nextToken(begin, end));
//					float y = nextFloat<std:
                //	this->Ka = std::make_tuple(x, nextFloat<x>(begin, end), nextFloat<x>(begin, end));
                }
                else
                {

                }
                break;
            }
            case hash(DIFFUSE_LIGHT_TOKEN):
                break;
            case hash(SPECULAR_LIGHT_TOKEN):
                break;
            case hash(TRANSMISSION_FILTER_TOKEN):
                break;
            case hash(ILLUMINATION_MODEL_TOKEN):
                break;
            case hash(DISSOLVE_TOKEN):
                break;
            case hash(SPECULAR_EXPONENT_TOKEN):
                break;
            case hash(SHARPNESS_TOKEN):
                break;
            case hash(OPTICAL_DENSITY_TOKEN):
                break;
            case hash(DIFFUSE_TEXTURE_TOKEN):
                break;
            case hash(AMBIENT_TEXTURE_TOKEN):
                break;
            case hash(SPECULAR_COLOR_TEXTURE_TOKEN):
                break;
            case hash(ALPHA_TEXTURE_TOKEN):
                break;
            case hash(BUMP_TEXTURE_TOKEN):
                break;
            case hash(DISPLACEMENT_TEXTURE_TOKEN):
                break;
            case hash(SPECULAR_SCALAR_TEXTURE_TOKEN):
                break;
            case hash(REFLECTION_TEXTURE_TOKEN):
                break;
            default:
                break;
        }
    }
}         */

}

#endif