/**
 * 
 * This is an internal file and should not be included directly by users.
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * This file contains all of the standard tokens in the Wavefront MTL ASCII format 
 * accompanied by the official documentation provided by Wavefront.  All documentation 
 * is under an implicit proprietary copyright by Wavefront unless otherwise noted.
 * 
 * @version 1.0, 3.22.2014
 * 
 * @since 1.0
 * 
 * @author Luke A. Leber
 * 
 */
#ifndef OBJ_MATERIAL_TOKENS_HPP
#    define OBJ_MATERIAL_TOKENS_HPP

namespace obj_detail
{
    constexpr const char* COMMENT_TOKEN = "#";

    /**
     * Each material description in an .mtl file consists of the newmtl 
     * statement, which assigns a name to the material and designates the start 
     * of a material description.  This statement is followed by the material 
     * color, texture map, and reflection map statements that describe the 
     * material.  An .mtl file map contain many different material 
     * descriptions.
     * 
     * Names may be any length but 
     * cannot include blanks.  Underscores may be used in material names.
     * 
     */
    constexpr const char* const NAME_STATEMENT_TOKEN = "newmtl";

    /**
     * To specify the ambient reflectivity of the current material, you can 
     * use the "Ka" statement, the "Ka spectral" statement, or the "Ka xyz" 
     * statement.
     *  
     *  Tip	These statements are mutually exclusive.  They cannot be used 
     * concurrently in the same material.
     *  
     *  Ka r g b
     *  
     *  The Ka statement specifies the ambient reflectivity using RGB values.
     *  
     *  "r g b" are the values for the red, green, and blue components of the 
     * color.  The g and b arguments are optional.  If only r is specified, 
     * then g, and b are assumed to be equal to r.  The r g b values are 
     * normally in the range of 0.0 to 1.0.  Values outside this range increase 
     * or decrease the relectivity accordingly.
     *  
     *  Ka spectral file.rfl factor
     *  
     *  The "Ka spectral" statement specifies the ambient reflectivity using a 
     * spectral curve.
     *  
     *  "file.rfl" is the name of the .rfl file.
     *  "factor" is an optional argument.
     *  "factor" is a multiplier for the values in the .rfl file and defaults 
     * to 1.0, if not specified.
     *  
     *  Ka xyz x y z
     *  
     *  The "Ka xyz" statement specifies the ambient reflectivity using CIEXYZ 
     * values.
     *  
     *  "x y z" are the values of the CIEXYZ color space.  The y and z 
     * arguments are optional.  If only x is specified, then y and z are 
     * assumed to be equal to x.  The x y z values are normally in the range of 
     * 0 to 1.  Values outside this range increase or decrease the reflectivity 
     * accordingly.
     * 
     */
    constexpr const char* const AMBIENT_LIGHT_TOKEN = "Ka";

    /**
     * To specify the diffuse reflectivity of the current material, you can 
     * use the "Kd" statement, the "Kd spectral" statement, or the "Kd xyz" 
     * statement.
     *  
     *  Tip	These statements are mutually exclusive.  They cannot be used 
     * concurrently in the same material.
     *  
     *  Kd r g b
     *  
     *  The Kd statement specifies the diffuse reflectivity using RGB values.
     *  
     *  "r g b" are the values for the red, green, and blue components of the 
     * atmosphere.  The g and b arguments are optional.  If only r is 
     * specified, then g, and b are assumed to be equal to r.  The r g b values 
     * are normally in the range of 0.0 to 1.0.  Values outside this range 
     * increase or decrease the relectivity accordingly.
     *  
     *  Kd spectral file.rfl factor
     *  
     *  The "Kd spectral" statement specifies the diffuse reflectivity using a 
     * spectral curve.
     *  
     *  "file.rfl" is the name of the .rfl file.
     *  "factor" is an optional argument.
     *  "factor" is a multiplier for the values in the .rfl file and defaults 
     * to 1.0, if not specified.
     *  
     *  Kd xyz x y z
     *  
     *  The "Kd xyz" statement specifies the diffuse reflectivity using CIEXYZ 
     * values.
     *  
     *  "x y z" are the values of the CIEXYZ color space.  The y and z 
     * arguments are optional.  If only x is specified, then y and z are 
     * assumed to be equal to x.  The x y z values are normally in the range of 
     * 0 to 1.  Values outside this range increase or decrease the reflectivity 
     * accordingly.
     * 
     */
    constexpr const char* const DIFFUSE_LIGHT_TOKEN = "Kd";

    /**
     * To specify the specular reflectivity of the current material, you can 
     * use the "Ks" statement, the "Ks spectral" statement, or the "Ks xyz" 
     * statement.
     *  
     *  Tip	These statements are mutually exclusive.  They cannot be used 
     * concurrently in the same material.
     *  
     *  Ks r g b
     *  
     *  The Ks statement specifies the specular reflectivity using RGB values.
     *  
     *  "r g b" are the values for the red, green, and blue components of the 
     * atmosphere.  The g and b arguments are optional.  If only r is 
     * specified, then g, and b are assumed to be equal to r.  The r g b values 
     * are normally in the range of 0.0 to 1.0.  Values outside this range 
     * increase or decrease the relectivity accordingly.
     *  
     *  Ks spectral file.rfl factor
     *  
     *  The "Ks spectral" statement specifies the specular reflectivity using a 
     * spectral curve.

     *  "file.rfl" is the name of the .rfl file.
     *  "factor" is an optional argument.
     *  "factor" is a multiplier for the values in the .rfl file and defaults 
     * to 1.0, if not specified.
     *  
     *  Ks xyz x y z
     *  
     *  The "Ks xyz" statement specifies the specular reflectivity using CIEXYZ 
     * values.
     *  
     *  "x y z" are the values of the CIEXYZ color space.  The y and z 
     * arguments are optional.  If only x is specified, then y and z are 
     * assumed to be equal to x.  The x y z values are normally in the range of 
     * 0 to 1.  Values outside this range increase or decrease the reflectivity 
     * accordingly.
     * 
     */
    constexpr const char* const SPECULAR_LIGHT_TOKEN = "Ks";

    /**
     * To specify the transmission filter of the current material, you can use 
     * the "Tf" statement, the "Tf spectral" statement, or the "Tf xyz" 
     * statement.
     *  
     *  Any light passing through the object is filtered by the transmission 
     * filter, which only allows the specifiec colors to pass through.  For 
     * example, Tf 0 1 0 allows all the green to pass through and filters out 
     * all the red and blue.
     *  
     *  Tip	These statements are mutually exclusive.  They cannot be used 
     * concurrently in the same material.
     *  
     *  Tf r g b
     *  
     *  The Tf statement specifies the transmission filter using RGB values.
     *  
     *  "r g b" are the values for the red, green, and blue components of the 
     * atmosphere.  The g and b arguments are optional.  If only r is 
     * specified, then g, and b are assumed to be equal to r.  The r g b values 
     * are normally in the range of 0.0 to 1.0.  Values outside this range 
     * increase or decrease the relectivity accordingly.
     *  
     *  Tf spectral file.rfl factor
     *  
     *  The "Tf spectral" statement specifies the transmission filterusing a 
     * spectral curve.
     *  
     *  "file.rfl" is the name of the .rfl file.
     *  "factor" is an optional argument.
     *  "factor" is a multiplier for the values in the .rfl file and defaults 
     * to 1.0, if not specified.
     *  
     *  Tf xyz x y z
     *  
     *  The "Ks xyz" statement specifies the specular reflectivity using CIEXYZ 
     * values.
     *  
     *  "x y z" are the values of the CIEXYZ color space.  The y and z 
     * arguments are optional.  If only x is specified, then y and z are 
     * assumed to be equal to x.  The x y z values are normally in the range of 
     * 0 to 1.  Values outside this range will increase or decrease the 
     * intensity of the light transmission accordingly.
     * 
     */
    constexpr const char* const TRANSMISSION_FILTER_TOKEN = "Tf";

    /**
     * The "illum" statement specifies the illumination model to use in the 
     * material.  Illumination models are mathematical equations that represent 
     * various material lighting and shading effects.
     *  
     *  "illum_#"can be a number from 0 to 10.  The illumination models are 
     * summarized below; for complete descriptions see "Illumination models" on 
     * page 5-30.
     *  
     *  Illumination    Properties that are turned on in the 
     *  model           Property Editor
     *  
     *  0		Color on and Ambient off
     *  1		Color on and Ambient on
     *  2		Highlight on
     *  3		Reflection on and Ray trace on
     *  4		Transparency: Glass on
     *  		Reflection: Ray trace on
     *  5		Reflection: Fresnel on and Ray trace on
     *  6		Transparency: Refraction on
     *  		Reflection: Fresnel off and Ray trace on
     *  7		Transparency: Refraction on
     *  		Reflection: Fresnel on and Ray trace on
     *  8		Reflection on and Ray trace off
     *  9		Transparency: Glass on
     *  		Reflection: Ray trace off
     *  10		Casts shadows onto invisible surfaces
     * 
     */
    constexpr const char* const ILLUMINATION_MODEL_TOKEN = "illum";

    /**
     * d factor
     *  
     *  Specifies the dissolve for the current material.
     *  
     *  "factor" is the amount this material dissolves into the background.  A 
     * factor of 1.0 is fully opaque.  This is the default when a new material 
     * is created.  A factor of 0.0 is fully dissolved (completely 
     * transparent).
     *  
     *  Unlike a real transparent material, the dissolve does not depend upon 
     * material thickness nor does it have any spectral character.  Dissolve 
     * works on all illumination models.
     *  
     *  d -halo factor
     *  
     *  Specifies that a dissolve is dependent on the surface orientation 
     * relative to the viewer.  For example, a sphere with the following 
     * dissolve, d -halo 0.0, will be fully dissolved at its center and will 
     * appear gradually more opaque toward its edge.
     *  
     *  "factor" is the minimum amount of dissolve applied to the material.  
     * The amount of dissolve will vary between 1.0 (fully opaque) and the 
     * specified "factor".  The formula is:
     *  
     *  dissolve = 1.0 - (N*v)(1.0-factor)
     *  
     *  For a definition of terms, see "Illumination models" on page 5-30.
     * 
     */
    constexpr const char* const DISSOLVE_TOKEN = "d";

    /**
     * Specifies the specular exponent for the current material.  This defines 
     * the focus of the specular highlight.
     *  
     *  "exponent" is the value for the specular exponent.  A high exponent 
     * results in a tight, concentrated highlight.  Ns values normally range 
     * from 0 to 1000.
     * 
     */
    constexpr const char* const SPECULAR_EXPONENT_TOKEN = "Ns";

    /**
     * Specifies the sharpness of the reflections from the local reflection 
     * map.  If a material does not have a local reflection map defined in its 
     * material definition, sharpness will apply to the global reflection map 
     * defined in PreView.
     *  
     *  "value" can be a number from 0 to 1000.  The default is 60.  A high 
     * value results in a clear reflection of objects in the reflection map.
     *  
     *  Tip	Sharpness values greater than 100 map introduce aliasing effects 
     * in flat surfaces that are viewed at a sharp angle
     * 
     */
    constexpr const char* const SHARPNESS_TOKEN = "sharpness";

    /**
     * Specifies the optical density for the surface.  This is also known as 
     * index of refraction.
     *  
     *  "optical_density" is the value for the optical density.  The values can 
     * range from 0.001 to 10.  A value of 1.0 means that light does not bend 
     * as it passes through an object.  Increasing the optical_density 
     * increases the amount of bending.  Glass has an index of refraction of 
     * about 1.5.  Values of less than 1.0 produce bizarre results and are not 
     * recommended.
     * 
     */
    constexpr const char* const OPTICAL_DENSITY_TOKEN = "Ni";

    /**
     * Specifies that a color texture file or color procedural texture file is 
     * linked to the diffuse reflectivity of the material.  During rendering, 
     * the map_Kd value is multiplied by the Kd value.
     *  
     *  "filename" is the name of a color texture file (.mpc), a color 
     * procedural texture file (.cxc), or an image file.
     *  
     *  The options for the map_Kd statement are listed below.  These options 
     * are described in detail in "Options for texture map statements" on page 
     * 5-18.
     *  
     *  	-blendu on | off
     *  	-blendv on | off
     *  	-cc on | off
     *  	-clamp on | off
     *  	-mm base gain
     *  	-o u v w
     *  	-s u v w
     *  	-t u v w
     *  	-texres value
     * 
     */
    constexpr const char* const DIFFUSE_TEXTURE_TOKEN = "map_kd";

    /**
     * Specifies that a color texture file or a color procedural texture file 
     * is applied to the ambient reflectivity of the material.  During 
     * rendering, the "map_Ka" value is multiplied by the "Ka" value.
     *  
     *  "filename" is the name of a color texture file (.mpc), a color 
     * procedural texture file (.cxc), or an image file.
     *  
     *  Tip	To make sure that the texture retains its original look, use the 
     * .rfl file "ident" as the underlying material.  This applies to the 
     * "map_Ka", "map_Kd", and "map_Ks" statements.  For more information on 
     * .rfl files, see chapter 8, "Spectral Curve File (.rfl)".
     *  
     *  The options for the "map_Ka" statement are listed below.  These options 
     * are described in detail in "Options for texture map statements" on page 
     * 5-18.
     *  
     *  	-blendu on | off
     *  	-blendv on | off
     *  	-cc on | off
     *  	-clamp on | off
     *  	-mm base gain
     *  	-o u v w
     *  	-s u v w
     *  	-t u v w
     *  	-texres value
     * 
     */
    constexpr const char* const AMBIENT_TEXTURE_TOKEN = "map_ka";

    /**
     * Specifies that a color texture file or color procedural texture file is 
     * linked to the specular reflectivity of the material.  During rendering, 
     * the map_Ks value is multiplied by the Ks value.
     *  
     *  "filename" is the name of a color texture file (.mpc), a color 
     * procedural texture file (.cxc), or an image file.
     *  
     *  The options for the map_Ks statement are listed below.  These options 
     * are described in detail in "Options for texture map statements" on page 
     * 5-18.
     *  
     *  	-blendu on | off
     *  	-blendv on | off
     *  	-cc on | off
     *  	-clamp on | off
     *  	-mm base gain
     *  	-o u v w
     *  	-s u v w
     *  	-t u v w
     *  	-texres value
     * 
     */
    constexpr const char* const SPECULAR_COLOR_TEXTURE_TOKEN = "map_ks";

    /**
     * Specifies that a scalar texture file or scalar procedural texture file 
     * is linked to the specular exponent of the material.  During rendering, 
     * the map_Ns value is multiplied by the Ns value.
     *  
     *  "filename" is the name of a scalar texture file (.mps), a scalar 
     * procedural texture file (.cxs), or an image file.
     *  
     *  The options for the map_Ns statement are listed below.  These options 
     * are described in detail in "Options for texture map statements" on page 
     * 5-18.
     *  
     *  	-blendu on | off
     *  	-blendv on | off
     *  	-clamp on | off
     *  	-imfchan r | g | b | m | l | z
     *  	-mm base gain
     *  	-o u v w
     *  	-s u v w
     *  	-t u v w
     *  	-texres value
     * 
     */
    constexpr const char* const SPECULAR_SCALAR_TEXTURE_TOKEN = "map_ns";

    /**
     * Specifies that a scalar texture file or scalar procedural texture file 
     * is linked to the dissolve of the material.  During rendering, the map_d 
     * value is multiplied by the d value.
     *  
     *  "filename" is the name of a scalar texture file (.mps), a scalar 
     * procedural texture file (.cxs), or an image file.
     *  
     *  The options for the map_d statement are listed below.  These options 
     * are described in detail in "Options for texture map statements" on page 
     * 5-18.
     *  
     *  	-blendu on | off
     *  	-blendv on | off
     *  	-clamp on | off
     *  	-imfchan r | g | b | m | l | z
     *  	-mm base gain
     *  	-o u v w
     *  	-s u v w
     *  	-t u v w
     *  	-texres value
     * 
     */
    constexpr const char* const ALPHA_TEXTURE_TOKEN = "map_d";

    /**
     * Turns on anti-aliasing of textures in this material without anti-
     * aliasing all textures in the scene.
     *  
     *  If you wish to selectively anti-alias textures, first insert this 
     * statement in the material file.  Then, when rendering with the Image 
     * panel, choose the anti-alias settings:  "shadows", "reflections 
     * polygons", or "polygons only".  If using Image from the command line, 
     * use the -aa or -os options.  Do not use the -aat option.
     *  
     *  Image will anti-alias all textures in materials with the map_aat on 
     * statement, using the oversampling level you choose when you run Image.  
     * Textures in other materials will not be oversampled.
     *  
     *  You cannot set a different oversampling level individually for each 
     * material, nor can you anti-alias some textures in a material and not 
     * others.  To anti-alias all textures in all materials, use the -aat 
     * option from the Image command line.  If a material with "map_aat on" 
     * includes a reflection map, all textures in that reflection map will be 
     * anti-aliased as well.
     *  
     *  You will not see the effects of map_aat in the Property Editor.
     *  
     *  Tip	Some .mpc textures map exhibit undesirable effects around the 
     * edges of smoothed objects.  The "map_aat" statement will correct this.
     * 
     */
    constexpr const char* const ANTI_ALIASING_TOKEN = "map_aat";

    /**
     * Specifies that a scalar texture file or a scalar procedural texture 
     * file is used to selectively replace the material color with the texture 
     * color.
     *  
     *  "filename" is the name of a scalar texture file (.mps), a scalar 
     * procedural texture file (.cxs), or an image file.
     *  
     *  During rendering, the Ka, Kd, and Ks values and the map_Ka, map_Kd, and 
     * map_Ks values are blended according to the following formula:
     *  
     *  result_color=tex_color(tv)*decal(tv)+mtl_color*(1.0-decal(tv))
     *  
     *  where tv is the texture vertex.
     *  
     *  "result_color" is the blended Ka, Kd, and Ks values.
     *  
     *  The options for the decal statement are listed below.  These options 
     * are described in detail in "Options for texture map statements" on page 
     * 5-18.
     *  
     *  	-blendu on | off
     *  	-blendv on | off
     *  	-clamp on | off
     *  	-imfchan r | g | b | m | l | z
     *  	-mm base gain
     *  	-o u v w
     *  	-s u v w
     *  	-t u v w
     *  	-texres value
     * 
     */
    constexpr const char* const DECAL_TOKEN = "decal";

    /**
     * Specifies that a scalar texture is used to deform the surface of an 
     * object, creating surface roughness.
     *  
     *  "filename" is the name of a scalar texture file (.mps), a bump 
     * procedural texture file (.cxb), or an image file.
     *  
     *  The options for the disp statement are listed below.  These options are 
     * described in detail in "Options for texture map statements" on page 5-
     * 18.
     *  
     *  	-blendu on | off
     *  	-blendv on | off
     *  	-clamp on | off
     *  	-imfchan r | g | b | m | l | z
     *  	-mm base gain
     *  	-o u v w
     *  	-s u v w
     *  	-t u v w
     *  	-texres value
     * 
     */
    constexpr const char* const DISPLACEMENT_TEXTURE_TOKEN = "disp";

    /**
     * Specifies that a bump texture file or a bump procedural texture file is 
     * linked to the material.
     *  
     *  "filename" is the name of a bump texture file (.mpb), a bump procedural 
     * texture file (.cxb), or an image file.
     *  
     *  The options for the bump statement are listed below.  These options are 
     * described in detail in "Options for texture map statements" on page 5-
     * 18.
     *  
     *  	-bm mult
     *  	-clamp on | off
     *  	-blendu on | off
     *  	-blendv on | off
     *  	-imfchan r | g | b | m | l | z
     *  	-mm base gain
     *  	-o u v w
     *  	-s u v w
     *  	-t u v w
     *  	-texres value
     * 
     */
    constexpr const char* const BUMP_TEXTURE_TOKEN = "bump";

    /**
     * A reflection map is an environment that simulates reflections in 
     * specified objects.  The environment is represented by a color texture 
     * file or procedural texture file that is mapped on the inside of an 
     * infinitely large, space.  Reflection maps can be spherical or cubic.  A 
     * spherical reflection map requires only one texture or image file, while 
     * a cubic reflection map requires six.
     *  
     *  Each material description can contain one reflection map statement that 
     * specifies a color texture file or a color procedural texture file to 
     * represent the environment.  The material itself must be assigned an 
     * illumination model of 3 or greater.
     *  
     *  The reflection map statement in the .mtl file defines a local 
     * reflection map.  That is, each material assigned to an object in a scene 
     * can have an individual reflection map.  In PreView, you can assign a 
     * global reflection map to an object and specify the orientation of the 
     * reflection map.  Rotating the reflection map creates the effect of 
     * animating reflections independently of object motion.  When you replace 
     * a global reflection map with a local reflection map, the local 
     * reflection map inherits the transformation of the global reflection map.
     *  
     *  Syntax
     *  
     *  The following syntax statements describe the reflection map statement 
     * for .mtl files.
     *  
     *  refl -type sphere -options -args filename
     *  
     *  Specifies an infinitely large sphere that casts reflections onto the 
     * material.  You specify one texture file.
     *  
     *  "filename" is the color texture file, color procedural texture file, or 
     * image file that will be mapped onto the inside of the shape.
     *  
     *  refl -type cube_side -options -args filenames
     *  
     *  Specifies an infinitely large sphere that casts reflections onto the 
     * material.  You can specify different texture files for the "top", 
     * "bottom", "front", "back", "left", and "right" with the following 
     * statements:
     *  
     *  refl -type cube_top
     *  refl -type cube_bottom
     *  refl -type cube_front
     *  refl -type cube_back
     *  refl -type cube_left
     *  refl -type cube_right
     *  
     *  "filenames" are the color texture files, color procedural texture 
     * files, or image files that will be mapped onto the inside of the shape.
     *  
     *  The "refl" statements for sphere and cube can be used alone or with 
     *  any combination of the following options.  The options and their 
     * arguments are inserted between "refl" and "filename".
     *  
     *  -blendu on | off
     *  -blendv on | off
     *  -cc on | off
     *  -clamp on | off
     *  -mm base gain
     *  -o u v w
     *  -s u v w
     *  -t u v w
     *  -texres value
     *  
     *  The options for the reflection map statement are described in detail in 
     * "Options for texture map statements" on page 18.
     * 
     */
    constexpr const char* const REFLECTION_TEXTURE_TOKEN = "refl";

    /**
     * The -blendu option turns texture blending in the horizontal direction 
     * (u direction) on or off.  The default is on.
     * 
     */
    constexpr const char* const BLEND_U_OPTION_TOKEN = "-blendu";

    /**
     * The -blendv option turns texture blending in the vertical direction (v 
     * direction) on or off.  The default is on.
     * 
     */
    constexpr const char* const BLEND_V_OPTION_TOKEN = "-blendv";

    /**
     * The -bm option specifies a bump multiplier.  You can use it only with 
     * the "bump" statement.  Values stored with the texture or procedural 
     * texture file are multiplied by this value before they are applied to the 
     * surface.

     *  "mult" is the value for the bump multiplier.  It can be positive or 
     * negative.  Extreme bump multipliers may cause odd visual results because 
     * only the surface normal is perturbed and the surface position does not 
     * change.  For best results, use values between 0 and 1.
     * 
     */
    constexpr const char* const BUMP_MULTIPLIER_OPTION_TOKEN = "-bm";

    /**
     * The -boost option increases the sharpness, or clarity, of mip-mapped 
     * texture files -- that is, color (.mpc), scalar (.mps), and bump (.mpb) 
     * files.  If you render animations with boost, you may experience some 
     * texture crawling.  The effects of boost are seen when you render in 
     * Image or test render in Model or PreView; they aren't as noticeable in 
     * Property Editor.
     *  
     * "value" is any non-negative floating point value representing the 
     * degree of increased clarity; the greater the value, the greater the 
     * clarity.  You should start with a boost value of no more than 1 or 2 and 
     * increase the value as needed.  Note that larger values have more 
     * potential to introduce texture crawling when animated.
     * 
     */
    constexpr const char* const BOOST_OPTION_TOKEN = "-boost";

    /**
     * The -cc option turns on color correction for the texture.  You can use 
     * it only with the color map statements:  map_Ka, map_Kd, and map_Ks.
     * 
     */
    constexpr const char* const COLOR_CORRECTION_OPTION_TOKEN = "-cc";

    /**
     * The -clamp option turns clamping on or off.  When clamping is on, 
     * textures are restricted to 0-1 in the uvw range.  The default is off.
     *  
     * When clamping is turned on, one copy of the texture is mapped onto the 
     * surface, rather than repeating copies of the original texture across the 
     * surface of a polygon, which is the default.  Outside of the origin 
     * texture, the underlying material is unchanged.
     *  
     * A postage stamp on an envelope or a label on a can of soup is an 
     * example of a texture with clamping turned on.  A tile floor or a 
     * sidewalk is an example of a texture with clamping turned off.
     *  
     * Two-dimensional textures are clamped in the u and v dimensions; 3D 
     * procedural textures are clamped in the u, v, and w dimensions.
     * 
     */
    constexpr const char* const CLAMP_OPTION_TOKEN = "-clamp";

    /**
     * The -imfchan option specifies the channel used to create a scalar or 
     * bump texture.  Scalar textures are applied to:
     *  
     *  transparency
     *  specular exponent
     *  decal
     *  displacement
     *  
     *  The channel choices are:
     *  
     *  r specifies the red channel.
     *  g specifies the green channel.
     *  b specifies the blue channel.
     *  m specifies the matte channel.
     *  l specifies the luminance channel.
     *  z specifies the z-depth channel.
     *   
     * The default for bump and scalar textures is "l" (luminance), unless you 
     * are building a decal.  In that case, the default is "m" (matte).
     * 
     */
    constexpr const char* const CHANNEL_OPTION_TOKEN = "-imfchan";

    /**
     * The -mm option modifies the range over which scalar or color texture 
     * values may vary.  This has an effect only during rendering and does not 
     * change the file.
     *  
     * "base" adds a base value to the texture values.  A positive value makes 
     * everything brighter; a negative value makes everything dimmer.  The 
     * default is 0; the range is unlimited.
     *  
     * "gain" expands the range of the texture values.  Increasing the number 
     * increases the contrast.  The default is 1; the range is unlimited.
     * 
     */
    constexpr const char* const MODIFY_MAP_OPTION_TOKEN = "-mm";

    /**
     * The -o option offsets the position of the texture map on the surface by 
     * shifting the position of the map origin.  The default is 0, 0, 0.
     * 
     * "u" is the value for the horizontal direction of the texture
     * 
     * "v" is an optional argument.
     * "v" is the value for the vertical direction of the texture.
     *  
     * "w" is an optional argument.
     * "w" is the value used for the depth of a 3D texture.
     * 
     */
    constexpr const char* const OFFSET_OPTION_TOKEN = "-o";

    /**
     * The -s option scales the size of the texture pattern on the textured 
     * surface by expanding or shrinking the pattern.  The default is 1, 1, 1.
     *  
     * "u" is the value for the horizontal direction of the texture
     *  
     * "v" is an optional argument.
     * "v" is the value for the vertical direction of the texture.
     *  
     * "w" is an optional argument.
     * "w" is a value used for the depth of a 3D texture.
     * "w" is a value used for the amount of tessellation of the displacement 
     * map.
     * 
     */
    constexpr const char* const SCALE_OPTION_TOKEN = "-s";

    /**
     * The -t option turns on turbulence for textures.  Adding turbulence to a 
     * texture along a specified direction adds variance to the original image 
     * and allows a simple image to be repeated over a larger area without 
     * noticeable tiling effects.
     *  
     * turbulence also lets you use a 2D image as if it were a solid texture, 
     * similar to 3D procedural textures like marble and granite.
     *  
     * "u" is the value for the horizontal direction of the texture 
     * turbulence.
     *  
     * "v" is an optional argument.
     * "v" is the value for the vertical direction of the texture turbulence.
     *  
     * "w" is an optional argument.
     * "w" is a value used for the depth of the texture turbulence.
     *  
     * By default, the turbulence for every texture map used in a material is 
     * uvw = (0,0,0).  This means that no turbulence will be applied and the 2D 
     * texture will behave normally.
     *  
     * Only when you raise the turbulence values above zero will you see the 
     * effects of turbulence.
     * 
     */
    constexpr const char* const TURBULENCE_OPTION_TOKEN = "-t";

    /**
     * The -texres option specifies the resolution of texture created when an 
     * image is used.  The default texture size is the largest power of two 
     * that does not exceed the original image size.
     *  
     * If the source image is an exact power of 2, the texture cannot be built 
     * any larger.  If the source image size is not an exact power of 2, you 
     * can specify that the texture be built at the next power of 2 greater 
     * than the source image size.
     *  
     * The original image should be square, otherwise, it will be scaled to 
     * fit the closest square size that is not larger than the original.  
     * Scaling reduces sharpness.
     * 
     */
    constexpr const char* const RESOLUTION_OPTION_TOKEN = "-texres";


    /**
     * A reflection map is an environment that simulates reflections in 
     * specified objects.  The environment is represented by a color texture 
     * file or procedural texture file that is mapped on the inside of an 
     * infinitely large, space.  Reflection maps can be spherical or cubic.  A 
     * spherical reflection map requires only one texture or image file, while 
     * a cubic reflection map requires six.
     * 
     * The formal specification was a bit vague...
     * 
     * -type should be either 'sphere' or 'cube_side'
     * 
     */
    constexpr const char* const TYPE_OPTION_TOKEN = "-type";
}
#endif
