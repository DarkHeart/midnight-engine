/**
 * 
 * This is an internal file and should not be included directly by users.
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * This file contains the functions that parse text files in OBJ format.
 * 
 * @version 1.0, 3.22.2014
 * 
 * @since 1.0
 * 
 * @author Luke A. Leber
 * 
 */
#ifndef OBJ_PARSER_HPP
#    define OBJ_PARSER_HPP

#    include <algorithm>            /// std::find_if

#    include "hash_code.hpp"        /// midnight::hash
#    include "obj_tokens.hpp"       /// OBJ format tokens
#    include "obj_format.hpp"       /// midnight::obj_detail::Face, midnight::obj_detail::Group, midnight::obj_detail::Mesh
#    include "obj_parser_commons.hpp"

namespace obj_detail
{

    /**
     * Inserts a Point3F into the provided buffer extracted from the provided range
     * 
     * @param buffer the buffer to insert into
     * 
     * @param begin the beginning of the range
     * 
     * @param end the end of the range
     * 
     */
    void readPoint3F(std::vector<midnight::Point3F>& buffer,
            std::string::iterator& begin,
            const std::string::const_iterator& end)
    {
        std::string line = restOfLine(begin, end);
        std::string::iterator lineBegin = line.begin();
        std::string::const_iterator lineEnd = line.end();
        buffer.push_back(midnight::Point3F(
                std::stof(nextToken(lineBegin, lineEnd)),
                std::stof(nextToken(lineBegin, lineEnd)),
                std::stof(nextToken(lineBegin, lineEnd))));
    }

    /**
     * Inserts a Vector3F into the provided buffer extracted from the provided range
     * 
     * @param buffer the buffer to insert into
     * 
     * @param begin the beginning of the range
     * 
     * @param end the end of the range
     * 
     */
    void readVector3F(std::vector<midnight::Vector3F>& buffer,
            std::string::iterator& begin,
            const std::string::const_iterator& end)
    {
        std::string line = restOfLine(begin, end);
        std::string::iterator lineBegin = line.begin();
        std::string::const_iterator lineEnd = line.end();
        buffer.push_back(midnight::Vector3F(
                std::stof(nextToken(lineBegin, lineEnd)),
                std::stof(nextToken(lineBegin, lineEnd)),
                std::stof(nextToken(lineBegin, lineEnd))));
    }

    /**
     * Inserts a Point2F into the provided buffer extracted from the provided range
     * 
     * @param buffer the buffer to insert into
     * 
     * @param begin the beginning of the range
     * 
     * @param end the end of the range
     * 
     */
    void readPoint2F(std::vector<midnight::Point2F>& buffer,
            std::string::iterator& begin,
            const std::string::const_iterator& end)
    {
        std::string line = restOfLine(begin, end);
        std::string::iterator lineBegin = line.begin();
        std::string::const_iterator lineEnd = line.end();
        buffer.push_back((midnight::Point2F(
                std::stof(nextToken(lineBegin, lineEnd)),
                std::stof(nextToken(lineBegin, lineEnd)))));
    }

    /**
     * Inserts a Vector2F into the provided buffer extracted from the provided range
     * 
     * @param buffer the buffer to insert into
     * 
     * @param begin the beginning of the range
     * 
     * @param end the end of the range
     * 
     */
    void readVector2F(std::vector<midnight::Vector2F>& buffer,
            std::string::iterator& begin,
            const std::string::const_iterator& end)
    {
        std::string line = restOfLine(begin, end);
        std::string::iterator lineBegin = line.begin();
        std::string::const_iterator lineEnd = line.end();
        buffer.push_back((midnight::Vector2F(
                std::stof(nextToken(lineBegin, lineEnd)),
                std::stof(nextToken(lineBegin, lineEnd)))));
    }

    /**
     * Inserts a Face into the provided buffer formed from the provided string
     * 
     * @param buffer the buffer to insert into
     * 
     * @param line the string that contains the face data
     * 
     */
    void readFace(std::vector<Face>& buffer, const std::string& line)
    {
        std::array<std::vector<std::size_t>, 3> data;
        std::string::const_iterator begin = line.begin();
        std::string::const_iterator end = line.end();

        std::size_t i = 0;
        std::string::const_iterator cursor = begin;
        do
        {
            /// Skip whitespace            
            while(*begin == '/' || isWhitespace(*begin)) ++begin;

            /// Was only whitespace left in the line?
            if(begin == end)
            {
                break;
            }

            /// advance cursor to the next slash or whitespace (or "end"))
            cursor = std::find_if(begin, end, [](char c)
            {
                return c == '/' || isWhitespace(c);
            });

            /// Grab a value
            data[i++].push_back((std::size_t)std::stoi(std::string(begin, cursor)));

            /// Are we ready to start reading a new vertex?
            if(isWhitespace(*cursor))
            {
                i = 0;
            }

            begin = cursor;
        }
        while(begin != end);
        buffer.push_back(Face(data[0], data[1], data[2]));
    }

    Mesh readOBJ(std::string::iterator begin, const std::string::const_iterator& end)
    {
        std::vector<midnight::Point3F> positions;
        std::vector<midnight::Vector3F> normals;
        std::vector<midnight::Point2F> texCoords;
        std::vector<Face> faces;
        std::vector<Group> groups;
        std::vector<Material> materials;

        while(begin != end)
        {
            switch(hashCode(nextToken(begin, end).c_str()))
            {
                case hashCode(OBJ_VERTEX_TOKEN):
                    readPoint3F(positions, begin, end);
                    std::cout << positions[0][0] << std::endl;
                    break;
                case hashCode(OBJ_NORMAL_TOKEN):
                    readVector3F(normals, begin, end);
                    break;
                case hashCode(OBJ_TEXCOORD_TOKEN):
                    readPoint2F(texCoords, begin, end);
                    break;
                case hashCode(OBJ_FACE_TOKEN):
                    readFace(faces, restOfLine(begin, end));
                    break;
                default:
                    break;
            }
        }
        return Mesh(materials, positions, texCoords, normals, faces, groups);
    }
}

#endif