/**
 * 
 * This is an internal file and should not be included directly by users.
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * This file contains parsing helper functions that are common to both OBJ and MTL formats.
 * 
 * @version 1.0, 3.22.2014
 * 
 * @since 1.0
 * 
 * @author Luke A. Leber
 * 
 */
#ifndef OBJ_PARSER_COMMONS
#    define OBJ_PARSER_COMMONS

namespace obj_detail
{

    /**
     * Is the provided character a line-continuation?
     * 
     * @param c the character to check
     * 
     * @return true if the character is a line-continuation character, otherwise false
     * 
     */
    constexpr inline bool isContinuation(char c) noexcept
    {
        return c == '\\';
    }

    /**
     * Does the provided character signal a line-end?
     * 
     * @param c the character to check
     * 
     * @return true if the character signals a line-end, otherwise false
     * 
     */
    constexpr inline bool isLineEnd(char c) noexcept
    {
        return c == '\n' || c == '\r';
    }

    /**
     * Does the provided character represent whitespace?
     * 
     * @param c the character to check
     * 
     * @return true if the character is whitespace, otherwise false
     * 
     */
    constexpr inline bool isWhitespace(char c) noexcept
    {
        return c == ' ' || c == '\t';
    }

    /**
     * Advances the provided 'begin' iterator to the first occurrence of non-whitespace (stopping at 'end' if the 
     * range is entirely whitespace)
     * 
     * @param begin the beginning iterator
     * 
     * @param end the end iterator
     * 
     */
    void skipWhitespace(std::string::iterator& begin, std::string::iterator& end) noexcept
    {
        while(begin != end && (*begin == ' ' || *begin == '\t'))
        {
            ++begin;
        }
    }

    /**
     * Retrieves a string that contains the first line of the provided range
     * 
     * @param begin the beginning iterator
     * 
     * @param end the end iterator
     * 
     * @return a string that contains the first line of the provided range
     * 
     * @note the returned string may be empty
     * 
     */
    std::string restOfLine(std::string::iterator& begin, const std::string::const_iterator& end) noexcept
    {
        // Early exit if the iterators are out of range
        if(begin == end)
        {
            return "";
        }
        std::string::iterator tokenStart = begin;
        std::string::iterator tokenEnd = begin;
        while(tokenEnd != end && (!isLineEnd(*tokenEnd) || isContinuation(*(tokenEnd - 1))))
        {
            ++tokenEnd;
        }
        begin += (tokenEnd - tokenStart);
        return std::string(tokenStart, tokenEnd);
    }

    /**
     * Retrieves a string that contains the next token in the provided range
     * 
     * @param begin the beginning iterator
     * 
     * @param end the end iterator
     * 
     * @return a string that contains the next token in the provided range
     * 
     * @note the returned string may be empty
     * 
     */
    std::string nextToken(std::string::iterator& begin, const std::string::const_iterator& end) noexcept
    {
        while(begin != end && (isLineEnd(*begin) || isWhitespace(*begin)))
        {
            ++begin;
        }
        if(begin == end)
        {
            return "";
        }
        std::string::iterator tokenStart = begin;
        std::string::iterator tokenEnd = begin;
        while(tokenEnd != end && !isLineEnd(*tokenEnd) && !isWhitespace(*tokenEnd))
        {
            ++tokenEnd;
        }
        begin += (tokenEnd - tokenStart);
        return std::string(tokenStart, tokenEnd);
    }

    /**
     * Counts the number of tokens in the provided range
     * 
     * @param begin the beginning of the range
     * 
     * @param end the end of the range
     * 
     * @return the number of tokens in the provided range
     * 
     */
    std::size_t tokenCount(std::string::iterator begin, const std::string::const_iterator& end) noexcept
    {
        std::size_t count = 0;
        while(begin != end)
        {
            nextToken(begin, end);
            ++count;
        }
        return count;
    }
}

#endif
