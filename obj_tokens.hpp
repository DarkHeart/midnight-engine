/**
 * 
 * This is an internal file and should not be included directly by users.
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * This file contains all of the standard tokens in the Wavefront OBJ ASCII format 
 * accompanied by the official documentation provided by Wavefront.  All documentation 
 * is under an implicit proprietary copyright by Wavefront unless otherwise noted.
 * 
 * @version 1.0, 3.22.2014
 * 
 * @since 1.0
 * 
 * @author Luke A. Leber
 * 
 */
#ifndef OBJ_TOKENS_HPP
#    define OBJ_TOKENS_HPP

namespace obj_detail
{

    /**
     * 
     * Comments can appear anywhere in an .obj file. They are used to annotate
     * the file; they are not processed.
     * 
     * Here is an example:
     * 
     *     # this is a comment
     * 
     */
    constexpr const char* OBJ_COMMENT_TOKEN = "#";

    /**
     * (Not Officially Documented)
     * 
     * Here is an example:
     * 
     *     v 0.000000 0.000000 \
     *     0.000000
     *
     */
    constexpr const char* OBJ_LINE_CONTINUATION_TOKEN = "\\";

    // Supported Vertex Data

    /**
     * 
     * v x y z w
     * 
     *     Polygonal and free-form geometry statement.
     * 
     *     Specifies a geometric vertex and its x y z coordinates. Rational
     *     curves and surfaces require a fourth homogeneous coordinate, also
     *     called the weight.
     * 
     *     x y z are the x, y, and z coordinates for the vertex. These are
     *     floating point numbers that define the position of the vertex in
     *     three dimensions.
     * 
     *     w is the weight required for rational curves and surfaces. It is
     *     not required for non-rational curves and surfaces. If you do not
     *     specify a value for w, the default is 1.0.
     * 
     *     NOTE: A positive weight value is recommended. Using zero or
     *     negative values may result in an undefined point in a curve or
     *     surface.
     * 
     */
    constexpr const char* OBJ_VERTEX_TOKEN = "v";

    /**
     * 
     * vn i j k
     * 
     *     Polygonal and free-form geometry statement.
     * 
     *     Specifies a normal vector with components i, j, and k.
     * 
     *     Vertex normals affect the smooth-shading and rendering of geometry.
     *     For polygons, vertex normals are used in place of the actual facet
     *     normals.  For surfaces, vertex normals are interpolated over the
     *     entire surface and replace the actual analytic surface normal.
     * 
     *     When vertex normals are present, they supersede smoothing groups.
     * 
     *     i j k are the i, j, and k coordinates for the vertex normal. They
     *     are floating point numbers.
     * 
     */
    constexpr const char* OBJ_NORMAL_TOKEN = "vn";

    /**
     * 
     * vt u v w
     * 
     *     Vertex statement for both polygonal and free-form geometry.
     * 
     *     Specifies a texture vertex and its coordinates. A 1D texture
     *     requires only u texture coordinates, a 2D texture requires both u
     *     and v texture coordinates, and a 3D texture requires all three
     *     coordinates.
     * 
     *     u is the value for the horizontal direction of the texture.
     * 
     *     v is an optional argument.
     * 
     *     v is the value for the vertical direction of the texture. The
     *     default is 0.
     * 
     *     w is an optional argument.
     * 
     *     w is a value for the depth of the texture. The default is 0.
     * 
     */
    constexpr const char* OBJ_TEXCOORD_TOKEN = "vt";

    // Unsupported Vertex Data

    /**
     * 
     * vp u v w
     * 
     *     Free-form geometry statement.
     * 
     *     Specifies a point in the parameter space of a curve or surface.
     * 
     *     The usage determines how many coordinates are required. Special
     *     points for curves require a 1D control point (u only) in the
     *     parameter space of the curve. Special points for surfaces require a
     *     2D point (u and v) in the parameter space of the surface. Control
     *     points for non-rational trimming curves require u and v
     *     coordinates. Control points for rational trimming curves require u,
     *     v, and w (weight) coordinates.
     * 
     *     u is the point in the parameter space of a curve or the first
     *     coordinate in the parameter space of a surface.
     * 
     *     v is the second coordinate in the parameter space of a surface.
     * 
     *     w is the weight required for rational trimming curves. If you do
     *     not specify a value for w, it defaults to 1.0.
     * 
     *     NOTE: For additional information on parameter vertices, see the
     *     curv2 and sp statements
     * 
     */
    constexpr const char* OBJ_PARAMETER_SPACE_VERTEX_TOKEN = "vp";

    /**
     * 
     * 
     * Specifying free-form curves/surfaces
     * 
     * There are three steps involved in specifying a free-form curve or
     * surface element.
     * 
     * o       Specify the type of curve or surface (basis matrix, Bezier,
     * 	B-spline, Cardinal, or Taylor) using free-form curve/surface
     * 	attributes.
     * 
     * o       Describe the curve or surface with element statements.
     * 
     * o       Supply additional information, using free-form curve/surface
     * 	body statements
     * 
     * The next three sections of this appendix provide detailed information
     * on each of these steps.
     * 
     * Data requirements for curves and surfaces
     * 
     * All curves and surfaces require a certain set of data. This consists of
     * the following:
     * 
     * Free-form curve/surface attributes
     * 
     * o       All curves and surfaces require type data, which is given with
     * 	the cstype statement.
     * 
     * o       All curves and surfaces require degree data, which is given
     * 	with the deg statement.
     * 
     * o       Basis matrix curves or surfaces require a bmat statement.
     * 
     * o       Basis matrix curves or surfaces also require a step size, which
     * 	is given with the step statement.
     * 
     * Elements
     * 
     * o       All curves and surfaces require control points, which are
     * 	referenced in the curv, curv2, or surf statements.
     * 
     * o       3D curves and surfaces require a parameter range, which is
     * 	given in the curv and surf statements, respectively.
     * 
     * Free-form curve/surface body statements
     * 
     * o       All curves and surfaces require a set of global parameters or a
     * 	knot vector, both of which are given with the parm statement.
     * 
     * o       All curves and surfaces body statements require an explicit end
     * 	statement.
     * 
     * Error checks
     * 
     * The above set of data starts out empty with no default values when
     * reading of an .obj file begins. While the file is being read,
     * statements are encountered, information is accumulated, and some errors
     * may be reported.
     * 
     * When the end statement is encountered, the following error checks,
     * which involve consistency between various statements, are performed:
     * 
     * o       All required information is present.
     * 
     * o       The number of control points, number of parameter values
     * 	(knots), and degree are consistent with the curve or surface
     * 	type. If the type is bmatrix, the step size is also consistent.
     * 	(For more information, refer to the parameter vector equations
     * 	in the section, "Mathematics of free-form curves/ surfaces" at
     * 	the end of appendix B1.)
     * 
     * o       If the type is bmatrix and the degree is n, the size of the
     * 	basis matrix is (n + 1) x (n + 1).
     * 
     * Note that any information given by the state-setting statements remains
     * in effect from one curve or surface to the next. Information given
     * within a curve or surface body is only effective for the curve or
     * surface it is given with.
     * 
     * 
     * 
     * Free-form curve/surface attributes
     * 
     * Five types of free-form geometry are available in the .obj file
     * format:
     * 
     * o       Bezier
     * 
     * o       basis matrix
     * 
     * o       B-spline
     * 
     * o       Cardinal
     * 
     * o       Taylor
     * 
     * You can apply these types only to curves and surfaces. Each of these
     * five types can be rational or non-rational.
     * 
     * In addition to specifying the type, you must define the degree for the
     * curve or surface. For basis matrix curve and surface elements, you must
     * also specify the basis matrix and step size.
     * 
     * All free-form curve and surface attribute statements are state-setting.
     * This means that once an attribute statement is set, it applies to all
     * elements that follow until it is reset to a different value.
     * 
     * Syntax
     * 
     * The following syntax statements are listed in order of use.
     * 
     * cstype rat type
     * 
     *     Free-form geometry statement.
     * 
     *     Specifies the type of curve or surface and indicates a rational or
     *     non-rational form.
     * 
     *     rat is an optional argument.
     * 
     *     rat specifies a rational form for the curve or surface type. If rat
     *     is not included, the curve or surface is non-rational
     * 
     *     type specifies the curve or surface type. Allowed types are:
     * 
     * 	bmatrix		basis matrix
     * 
     * 	bezier		Bezier
     * 
     * 	bspline		B-spline
     * 
     * 	cardinal        Cardinal
     * 
     * 	taylor		Taylor
     * 
     *     There is no default. A value must be supplied.
     * 
     */
    constexpr const char* OBJ_SURFACE_CURVE_TOKEN = "cstype";

    /**
     * 
     * deg degu degv
     * 
     *     Free-form geometry statement.
     * 
     *     Sets the polynomial degree for curves and surfaces.
     * 
     *     degu is the degree in the u direction. It is required for both
     *     curves and surfaces.
     * 
     *     degv is the degree in the v direction. It is required only for
     *     surfaces. For Bezier, B-spline, Taylor, and basis matrix, there is
     *     no default; a value must be supplied. For Cardinal, the degree is
     *     always 3. If some other value is given for Cardinal, it will be
     *     ignored.
     * 
     */
    constexpr const char* OBJ_DEGREE_TOKEN = "deg";

    /**
     * 
     * bmat u matrix
     * 
     * bmat v matrix
     * 
     *     Free-form geometry statement.
     * 
     *     Sets the basis matrices used for basis matrix curves and surfaces.
     *     The u and v values must be specified in separate bmat statements.
     * 
     *     NOTE: The deg statement must be given before the bmat statements
     *     and the size of the matrix must be appropriate for the degree.
     * 
     *     u specifies that the basis matrix is applied in the u direction.
     * 
     *     v specifies that the basis matrix is applied in the v direction.
     * 
     *     matrix lists the contents of the basis matrix with column subscript
     *     j varying the fastest. If n is the degree in the given u or v
     *     direction, the matrix (i,j) should be of size (n + 1) x (n + 1).
     * 
     *     There is no default. A value must be supplied.
     * 
     *     NOTE: The arrangement of the matrix is different from that commonly
     *     found in other references. For more information, see the examples
     *     at the end of this section and also the section, "Mathematics for
     *     free-form curves and surfaces."
     * 
     */
    constexpr const char* OBJ_BASIS_MATRIX_TOKEN = "bmat";

    /**
     * 
     * step stepu stepv
     * 
     *     Free-form geometry statement.
     * 
     *     Sets the step size for curves and surfaces that use a basis
     *     matrix.
     * 
     *     stepu is the step size in the u direction. It is required for both
     *     curves and surfaces that use a basis matrix.
     * 
     *     stepv is the step size in the v direction. It is required only for
     *     surfaces that use a basis matrix. There is no default. A value must
     *     be supplied.
     * 
     *     When a curve or surface is being evaluated and a transition from
     *     one segment or patch to the next occurs, the set of control points
     *     used is incremented by the step size. The appropriate step size
     *     depends on the representation type, which is expressed through the
     *     basis matrix, and on the degree.
     * 
     *     That is, suppose we are given a curve with k control points:
     * 			{v , ... v }
     * 			  1       k
     * 
     *     If the curve is of degree n, then n + 1 control points are needed
     *     for each polynomial segment. If the step size is given as s, then
     *     the ith polynomial segment, where i = 0 is the first segment, will
     *     use the control points:
     * 			{v    ,...,v      }
     * 			  is+1      is+n+1
     * 
     *     For example, for Bezier curves, s = n .
     * 
     *     For surfaces, the above description applies independently to each
     *     parametric direction.
     * 
     *     When you create a file which uses the basis matrix type, be sure to
     *     specify a step size appropriate for the current curve or surface
     *     representation.
     * 
     * Examples
     * 
     * 1.      Cubic Bezier surface made with a basis matrix
     * 
     *     To create a cubic Bezier surface:
     * 
     * 	cstype bmatrix
     * 	deg 3 3
     * 	step 3 3
     * 	bmat u  1       -3      3       -1      \
     *			0       3       -6      3       \
     *			0       0       3       -3      \
     *			0       0       0       1
     * 	bmat v  1       -3      3       -1      \
     *			0       3       -6      3       \
     *			0       0       3       -3      \
     *			0       0       0       1
     * 
     * 2.      Hermite curve made with a basis matrix
     * 
     *     To create a Hermite curve:
     * 
     * 	cstype bmatrix
     * 	deg 3
     * 	step 2
     * 	bmat u  1     0     -3      2      0       0       3      -2 \
     *			0     1     -2      1      0       0      -1       1
     * 
     * 3.      Bezier in u direction with B-spline in v direction;
     * 	made with a basis matrix
     * 
     *     To create a surface with a cubic Bezier in the u direction and
     *     cubic uniform B-spline in the v direction:
     * 
     * 	cstype bmatrix
     * 	deg 3 3
     * 	step 3 1
     * 	bmat u  1      -3       3      -1 \
     *			0       3      -6       3 \
     *			0       0       3      -3 \
     *			0       0       0       1
     * 	bmat v  0.16666 -0.50000  0.50000 -0.16666 \
     *			0.66666  0.00000 -1.00000  0.50000 \
     *			0.16666  0.50000  0.50000 -0.50000 \
     *			0.00000  0.00000  0.00000  0.16666
     * 
     */
    constexpr const char* OBJ_STEP_SIZE_TOKEN = "step";

    // Supported Elements

    /**
     * 
     * f  v1/vt1/vn1   v2/vt2/vn2   v3/vt3/vn3 . . .
     * 
     *     Polygonal geometry statement.
     * 
     *     Specifies a face element and its vertex reference number. You can
     *     optionally include the texture vertex and vertex normal reference
     *     numbers.
     * 
     *     The reference numbers for the vertices, texture vertices, and
     *     vertex normals must be separated by slashes (/). There is no space
     *     between the number and the slash.
     * 
     *     v is the reference number for a vertex in the face element. A
     *     minimum of three vertices are required.
     * 
     *     vt is an optional argument.
     * 
     *     vt is the reference number for a texture vertex in the face
     *     element. It always follows the first slash.
     * 
     *     vn is an optional argument.
     * 
     *     vn is the reference number for a vertex normal in the face element.
     *     It must always follow the second slash.
     * 
     *     Face elements use surface normals to indicate their orientation. If
     *     vertices are ordered counterclockwise around the face, both the
     *     face and the normal will point toward the viewer. If the vertex
     *     ordering is clockwise, both will point away from the viewer. If
     *     vertex normals are assigned, they should point in the general
     *     direction of the surface normal, otherwise unpredictable results
     *     may occur.
     * 
     *     If a face has a texture map assigned to it and no texture vertices
     *     are assigned in the f statement, the texture map is ignored when
     *     the element is rendered.
     * 
     *     NOTE: Any references to fo (face outline) are no longer valid as of
     *     version 2.11. You can use f (face) to get the same results.
     *     References to fo in existing .obj files will still be read,
     *     however, they will be written out as f when the file is saved.
     * 
     */
    constexpr const char* OBJ_FACE_TOKEN = "f";

    // Unsupported Elements

    /**
     * 
     * p  v1 v2 v3 . . .
     * 
     *     Polygonal geometry statement.
     * 
     *     Specifies a point element and its vertex. You can specify multiple
     *     points with this statement. Although points cannot be shaded or
     *     rendered, they are used by other Advanced Visualizer programs.
     * 
     *     v is the vertex reference number for a point element. Each point
     *     element requires one vertex. Positive values indicate absolute
     *     vertex numbers. Negative values indicate relative vertex numbers.
     * 
     */
    constexpr const char* OBJ_POINT_TOKEN = "p";

    /**
     * 
     * l  v1/vt1   v2/vt2   v3/vt3 . . .
     * 
     *     Polygonal geometry statement.
     * 
     *     Specifies a line and its vertex reference numbers. You can
     *     optionally include the texture vertex reference numbers. Although
     *     lines cannot be shaded or rendered, they are used by other Advanced
     *     Visualizer programs.
     * 
     *     The reference numbers for the vertices and texture vertices must be
     *     separated by a slash (/). There is no space between the number and
     *     the slash.
     * 
     *     v is a reference number for a vertex on the line. A minimum of two
     *     vertex numbers are required. There is no limit on the maximum.
     *     Positive values indicate absolute vertex numbers. Negative values
     *     indicate relative vertex numbers.
     * 
     *     vt is an optional argument.
     * 
     *     vt is the reference number for a texture vertex in the line
     *     element. It must always follow the first slash.
     * 
     */
    constexpr const char* OBJ_LINE_TOKEN = "l";

    /**
     * 
     * curv u0 u1 v1 v2 . . .
     * 
     *     Element statement for free-form geometry.
     * 
     *     Specifies a curve, its parameter range, and its control vertices.
     *     Although curves cannot be shaded or rendered, they are used by
     *     other Advanced Visualizer programs.
     * 
     *     u0 is the starting parameter value for the curve. This is a
     *     floating point number.
     * 
     *     u1 is the ending parameter value for the curve. This is a floating
     *     point number.
     * 
     *     v is the vertex reference number for a control point. You can
     *     specify multiple control points. A minimum of two control points
     *     are required for a curve.
     * 
     *     For a non-rational curve, the control points must be 3D. For a
     *     rational curve, the control points are 3D or 4D. The fourth
     *     coordinate (weight) defaults to 1.0 if omitted.
     * 
     */
    constexpr const char* OBJ_CURVE_TOKEN = "curv";

    /**
     * 
     * curv2  vp1  vp2   vp3. . .
     * 
     *     Free-form geometry statement.
     * 
     *     Specifies a 2D curve on a surface and its control points. A 2D
     *     curve is used as an outer or inner trimming curve, as a special
     *     curve, or for connectivity.
     * 
     *     vp is the parameter vertex reference number for the control point.
     *     You can specify multiple control points. A minimum of two control
     *     points is required for a 2D curve.
     * 
     *     The control points are parameter vertices because the curve must
     *     lie in the parameter space of some surface. For a non-rational
     *     curve, the control vertices can be 2D. For a rational curve, the
     *     control vertices can be 2D or 3D. The third coordinate (weight)
     *     defaults to 1.0 if omitted.
     * 
     */
    constexpr const char* OBJ_CURVE_2D_TOKEN = "curv2";

    /**
     * 
     * surf  s0  s1  t0  t1  v1/vt1/vn1   v2/vt2/vn2 . . .
     * 
     *     Element statement for free-form geometry.
     * 
     *     Specifies a surface, its parameter range, and its control vertices.
     *     The surface is evaluated within the global parameter range from s0
     *     to s1 in the u direction and t0 to t1 in the v direction.
     * 
     *     s0 is the starting parameter value for the surface in the u
     *     direction.
     * 
     *     s1 is the ending parameter value for the surface in the u
     *     direction.
     * 
     *     t0 is the starting parameter value for the surface in the v
     *     direction.
     * 
     *     t1 is the ending parameter value for the surface in the v
     *     direction.
     * 
     *     v is the reference number for a control vertex in the surface.
     * 
     *     vt is an optional argument.
     * 
     *     vt is the reference number for a texture vertex in the surface.  It
     *     must always follow the first slash.
     * 
     *     vn is an optional argument.
     * 
     *     vn is the reference number for a vertex normal in the surface.  It
     *     must always follow the second slash.
     * 
     *     For a non-rational surface, the control vertices are 3D.  For a
     *     rational surface the control vertices can be 3D or 4D.  The fourth
     *     coordinate (weight) defaults to 1.0 if ommitted.
     * 
     *     NOTE: For more information on the ordering of control points for
     *     survaces, refer to the section on surfaces and control points in
     *     "mathematics of free-form curves/surfaces" at the end of this
     *     appendix.
     * 
     */
    constexpr const char* OBJ_SURFACE_TOKEN = "surf";

    // Supported Free-form body statements

    // Unsupported Free-form body statements

    /**
     * 
     * 
     * parm u p1 p2 p3. . .
     * 
     * parm v p1 p2 p3 . . .
     * 
     *     Body statement for free-form geometry.
     * 
     *     Specifies global parameter values. For B-spline curves and
     *     surfaces, this specifies the knot vectors.
     * 
     *     u is the u direction for the parameter values.
     * 
     *     v is the v direction for the parameter values.
     * 
     *     To set u and v values, use separate command lines.
     * 
     *     p is the global parameter or knot value. You can specify multiple
     *     values. A minimum of two parameter values are required. Parameter
     *     values must increase monotonically. The type of surface and the
     *     degree dictate the number of values required.
     * 
     */
    constexpr const char* OBJ_PARAMETER_VALUE_TOKEN = "parm";

    /**
     * 
     * trim  u0  u1  curv2d  u0  u1  curv2d . . .
     * 
     *     Body statement for free-form geometry.
     * 
     *     Specifies a sequence of curves to build a single outer trimming
     *     loop.
     * 
     *     u0 is the starting parameter value for the trimming curve curv2d.
     * 
     *     u1 is the ending parameter value for the trimming curve curv2d.
     * 
     *     curv2d is the index of the trimming curve lying in the parameter
     *     space of the surface. This curve must have been previously defined
     *     with the curv2 statement.
     * 
     */
    constexpr const char* OBJ_OUTER_TRIMMING_LOOP_TOKEN = "trim";

    /**
     * 
     * hole  u0  u1  curv2d  u0  u1  curv2d . . .
     * 
     *     Body statement for free-form geometry.
     * 
     *     Specifies a sequence of curves to build a single inner trimming
     *     loop (hole).
     * 
     *     u0 is the starting parameter value for the trimming curve curv2d.
     * 
     *     u1 is the ending parameter value for the trimming curve curv2d.
     * 
     *     curv2d is the index of the trimming curve lying in the parameter
     *     space of the surface. This curve must have been previously defined
     *     with the curv2 statement.
     * 
     */
    constexpr const char* OBJ_INNER_TRIMMING_LOOP_TOKEN = "hole";

    /**
     * 
     * scrv u0 u1 curv2d u0 u1 curv2d . . .
     * 
     *     Body statement for free-form geometry.
     * 
     *     Specifies a sequence of curves which lie on the given surface to
     *     build a single special curve.
     * 
     *     u0 is the starting parameter value for the special curve curv2d.
     * 
     *     u1 is the ending parameter value for the special curve curv2d.
     * 
     *     curv2d is the index of the special curve lying in the parameter
     *     space of the surface. This curve must have been previously defined
     *     with the curv2 statement.
     * 
     */
    constexpr const char* OBJ_SPECIAL_CURVE_TOKEN = "scrv";

    /**
     * 
     * sp vp1  vp. . .
     * 
     *     Body statement for free-form geometry.
     * 
     *     Specifies special geometric points to be associated with a curve or
     *     surface. For space curves and trimming curves, the parameter
     *     vertices must be 1D. For surfaces, the parameter vertices must be
     *     2D.
     * 
     *     vp is the reference number for the parameter vertex of a special
     *     point to be associated with the parameter space point of the curve
     *     or surface.
     * 
     */
    constexpr const char* OBJ_SPECIAL_POINT_TOKEN = "sp";

    /**
     * 
     * end
     * 
     *     Body statement for free-form geometry.
     * 
     *     Specifies the end of a curve or surface body begun by a curv,
     *     curv2, or surf statement.
     * 
     */
    constexpr const char* OBJ_END_STATEMENT_TOKEN = "end";

    // Supported Connectivity statements

    // Unsupported Connectivity statements

    /**
     * 
     * con  surf_1  q0_1  q1_1   curv2d_1   surf_2  q0_2  q1_2  curv2d_2
     * 
     *     Free-form geometry statement.
     * 
     *     Specifies connectivity between two surfaces.
     * 
     *     surf_1 is the index of the first surface.
     * 
     *     q0_1 is the starting parameter for the curve referenced by
     *     curv2d_1.
     * 
     *     q1_1 is the ending parameter for the curve referenced by curv2d_1.
     * 
     *     curv2d_1 is the index of a curve on the first surface. This curve
     *     must have been previously defined with the curv2 statement.
     * 
     *     surf_2 is the index of the second surface.
     * 
     *     q0_2 is the starting parameter for the curve referenced by
     *     curv2d_2.
     * 
     *     q1_2 is the ending parameter for the curve referenced by curv2d_2.
     * 
     *     curv2d_2 is the index of a curve on the second surface. This curve
     *     must have been previously defined with the curv2 statement.
     * 
     */
    constexpr const char* OBJ_CONNECT_TOKEN = "con";

    // Supported Grouping

    /**
     * 
     * g group_name1 group_name2 . . .
     * 
     *     Polygonal and free-form geometry statement.
     * 
     *     Specifies the group name for the elements that follow it. You can
     *     have multiple group names. If there are multiple groups on one
     *     line, the data that follows belong to all groups. Group information
     *     is optional.
     * 
     *     group_name is the name for the group. Letters, numbers, and
     *     combinations of letters and numbers are accepted for group names.
     *     The default group name is default.
     * 
     */
    constexpr const char* OBJ_GROUP_NAME_TOKEN = "g";

    // Unsupported Grouping

    /**
     * 
     * s group_number
     * 
     *     Polygonal and free-form geometry statement.
     * 
     *     Sets the smoothing group for the elements that follow it. If you do
     *     not want to use a smoothing group, specify off or a value of 0.
     * 
     *     To display with smooth shading in Model and PreView, you must
     *     create vertex normals after you have assigned the smoothing groups.
     *     You can create vertex normals with the vn statement or with the
     *     Model program.
     * 
     *     To smooth polygonal geometry for rendering with Image, it is
     *     sufficient to put elements in some smoothing group. However, vertex
     *     normals override smoothing information for Image.
     * 
     *     group_number is the smoothing group number. To turn off smoothing
     *     groups, use a value of 0 or off. Polygonal elements use group
     *     numbers to put elements in different smoothing groups. For
     *     free-form surfaces, smoothing groups are either turned on or off;
     *     there is no difference between values greater than 0.
     * 
     */
    constexpr const char* OBJ_SMOOTHING_GROUP_TOKEN = "s";

    /**
     * 
     * mg group_number res
     * 
     *     Free-form geometry statement.
     * 
     *     Sets the merging group and merge resolution for the free-form
     *     surfaces that follow it. If you do not want to use a merging group,
     *     specify off or a value of 0.
     * 
     *     Adjacency detection is performed only within groups, never between
     *     groups. Connectivity between surfaces in different merging groups
     *     is not allowed. Surfaces in the same merging group are merged
     *     together along edges that are within the distance res apart.
     * 
     *     NOTE: Adjacency detection is an expensive numerical comparison
     *     process.  It is best to restrict this process to as small a domain
     *     as possible by using small merging groups.
     * 
     *     group_number is the merging group number. To turn off adjacency
     *     detection, use a value of 0 or off.
     * 
     *     res is the maximum distance between two surfaces that will be
     *     merged together. The resolution must be a value greater than 0.
     *     This is a required argument only when using merging groups.
     * 
     */
    constexpr const char* OBJ_MERGING_GROUP_TOKEN = "mg";

    /**
     * 
     * o object_name
     * 
     *     Polygonal and free-form geometry statement.
     * 
     *     Optional statement; it is not processed by any Wavefront programs.
     *     It specifies a user-defined object name for the elements defined
     *     after this statement.
     * 
     *     object_name is the user-defined object name. There is no default.
     * 
     */
    constexpr const char* OBJ_OBJECT_NAME_TOKEN = "o";


    // Supported Display/Render Attributes

    /**
     * 
     * usemtl material_name
     * 
     *     Polygonal and free-form geometry statement.
     * 
     *     Specifies the material name for the element following it. Once a
     *     material is assigned, it cannot be turned off; it can only be
     *     changed.
     * 
     *     material_name is the name of the material. If a material name is
     *     not specified, a white material is used.
     * 
     */
    constexpr const char* OBJ_MATERIAL_NAME_TOKEN = "usemtl";

    /**
     * 
     * mtllib filename1 filename2 . . .
     * 
     *     Polygonal and free-form geometry statement.
     * 
     *     Specifies the material library file for the material definitions
     *     set with the usemtl statement. You can specify multiple filenames
     *     with mtllib. If multiple filenames are specified, the first file
     *     listed is searched first for the material definition, the second
     *     file is searched next, and so on.
     * 
     *     When you assign a material library using the Model program, only
     *     one map library per .obj file is allowed. You can assign multiple
     *     libraries using a text editor.
     * 
     *     filename is the name of the library file that defines the
     *     materials.  There is no default.
     * 
     */
    constexpr const char* OBJ_MATERIAL_LIBRARY_TOKEN = "mtllib";

    // Unsupported Display/Render Attributes

    /**
     * 
     * bevel on/off
     * 
     *     Polygonal geometry statement.
     * 
     *     Sets bevel interpolation on or off. It works only with beveled
     *     objects, that is, objects with sides separated by beveled faces.
     * 
     *     Bevel interpolation uses normal vector interpolation to give an
     *     illusion of roundness to a flat bevel. It does not affect the
     *     smoothing of non-bevelled faces.
     * 
     *     Bevel interpolation does not alter the geometry of the original
     *     object.
     * 
     *     on turns on bevel interpolation.
     * 
     *     off turns off bevel interpolation. The default is off.
     * 
     *     NOTE: Image cannot render bevel-interpolated elements that have
     *     vertex normals.
     * 
     */
    constexpr const char* OBJ_BEVEL_INTERPOLATION_TOKEN = "bevel";

    /**
     * 
     * c_interp on/off
     * 
     *     Polygonal geometry statement.
     * 
     *     Sets color interpolation on or off.
     * 
     *     Color interpolation creates a blend across the surface of a polygon
     *     between the materials assigned to its vertices. This creates a
     *     blending of colors across a face element.
     * 
     *     To support color interpolation, materials must be assigned per
     *     vertex, not per element. The illumination models for all materials
     *     of vertices attached to the polygon must be the same. Color
     *     interpolation applies to the values for ambient (Ka), diffuse (Kd),
     *     specular (Ks), and specular highlight (Ns) material properties.
     * 
     *     on turns on color interpolation.
     * 
     *     off turns off color interpolation. The default is off.
     * 
     */
    constexpr const char* OBJ_COLOR_INTERPOLATION_TOKEN = "c_interp";

    /**
     * 
     * d_interp on/off
     * 
     *     Polygonal geometry statement.
     * 
     *     Sets dissolve interpolation on or off.
     * 
     *     Dissolve interpolation creates an interpolation or blend across a
     *     polygon between the dissolve (d) values of the materials assigned
     *     to its vertices. This feature is used to create effects exhibiting
     *     varying degrees of apparent transparency, as in glass or clouds.
     * 
     *     To support dissolve interpolation, materials must be assigned per
     *     vertex, not per element. All the materials assigned to the vertices
     *     involved in the dissolve interpolation must contain a dissolve
     *     factor command to specify a dissolve.
     * 
     *     on turns on dissolve interpolation.
     * 
     *     off turns off dissolve interpolation. The default is off.
     * 
     */
    constexpr const char* OBJ_DISSOLVE_INTERPOLATION_TOKEN = "d_interp";

    /**
     * 
     * lod level
     * 
     *     Polygonal and free-form geometry statement.
     * 
     *     Sets the level of detail to be displayed in a PreView animation.
     *     The level of detail feature lets you control which elements of an
     *     object are displayed while working in PreView.
     * 
     *     level is the level of detail to be displayed. When you set the
     *     level of detail to 0 or omit the lod statement, all elements are
     *     displayed.  Specifying an integer between 1 and 100 sets the level
     *     of detail to be displayed when reading the .obj file.
     * 
     */
    constexpr const char* OBJ_LEVEL_OF_DETAIL_TOKEN = "lod";

    /**
     * 
     * maplib filename1 filename2 . . .
     * 
     *     This is a rendering identifier that specifies the map library file
     *     for the texture map definitions set with the usemap identifier. You
     *     can specify multiple filenames with maplib. If multiple filenames
     *     are specified, the first file listed is searched first for the map
     *     definition, the second file is searched next, and so on.
     * 
     *     When you assign a map library using the Model program, Model allows
     *     only one map library per .obj file. You can assign multiple
     *     libraries using a text editor.
     * 
     *     filename is the name of the library file where the texture maps are
     *     defined. There is no default.
     * 
     */
    constexpr const char* OBJ_MAP_LIBRARY_TOKEN = "maplib";

    /**
     * 
     * usemap map_name/off
     * 
     *     This is a rendering identifier that specifies the texture map name
     *     for the element following it. To turn off texture mapping, specify
     *     off instead of the map name.
     * 
     *     If you specify texture mapping for a face without texture vertices,
     *     the texture map will be ignored.
     * 
     *     map_name is the name of the texture map.
     * 
     *     off turns off texture mapping. The default is off.
     * 
     */
    constexpr const char* OBJ_USE_MAP_TOKEN = "usemap";

    /**
     * 
     * shadow_obj filename
     * 
     *     Polygonal and free-form geometry statement.
     * 
     *     Specifies the shadow object filename. This object is used to cast
     *     shadows for the current object. Shadows are only visible in a
     *     rendered image; they cannot be seen using hardware shading. The
     *     shadow object is invisible except for its shadow.
     * 
     *     An object will cast shadows only if it has a shadow object. You can
     *     use an object as its own shadow object. However, a simplified
     *     version of the original object is usually preferable for shadow
     *     objects, since shadow casting can greatly increase rendering time.
     * 
     *     filename is the filename for the shadow object. You can enter any
     *     valid object filename for the shadow object. The object file can be
     *     an .obj or .mod file. If a filename is given without an extension,
     *     an extension of .obj is assumed.
     * 
     *     Only one shadow object can be stored in a file. If more than one
     *     shadow object is specified, the last one specified will be used.
     * 
     */
    constexpr const char* OBJ_SHADOW_CASTING_TOKEN = "shadow_obj";

    /**
     * 
     * trace_obj filename
     * 
     *     Polygonal and free-form geometry statement.
     * 
     *     Specifies the ray tracing object filename. This object will be used
     *     in generating reflections of the current object on reflective
     *     surfaces.  Reflections are only visible in a rendered image; they
     *     cannot be seen using hardware shading.
     * 
     *     An object will appear in reflections only if it has a trace object.
     *     You can use an object as its own trace object. However, a
     *     simplified version of the original object is usually preferable for
     *     trace objects, since ray tracing can greatly increase rendering
     *     time.
     * 
     *     filename is the filename for the ray tracing object. You can enter
     *     any valid object filename for the trace object. You can enter any
     *     valid object filename for the shadow object. The object file can be
     *     an .obj or .mod file. If a filename is given without an extension,
     *     an extension of .obj is assumed.
     * 
     *     Only one trace object can be stored in a file. If more than one is
     *     specified, the last one is used.
     * 
     */
    constexpr const char* OBJ_RAY_TRACING_TOKEN = "trace_obj";

    /**
     * 
     * ctech  technique  resolution
     * 
     *     Free-form geometry statement.
     * 
     *     Specifies a curve approximation technique. The arguments specify
     *     the technique and resolution for the curve.
     * 
     *     You must select from one of the following three techniques.
     * 
     *     ctech cparm res
     * 
     * 	Specifies a curve with constant parametric subdivision using
     * 	one resolution parameter. Each polynomial segment of the curve
     * 	is subdivided n times in parameter space, where n is the
     * 	resolution parameter multiplied by the degree of the curve.
     * 
     * 	res is the resolution parameter. The larger the value, the
     * 	finer the resolution. If res has a value of 0, each polynomial
     * 	curve segment is represented by a single line segment.
     * 
     *     ctech cspace maxlength
     * 
     * 	Specifies a curve with constant spatial subdivision. The curve
     * 	is approximated by a series of line segments whose lengths in
     * 	real space are less than or equal to the maxlength.
     * 
     * 	maxlength is the maximum length of the line segments. The
     * 	smaller the value, the finer the resolution.
     * 
     *     ctech curv maxdist maxangle
     * 
     * 	Specifies curvature-dependent subdivision using separate
     * 	resolution parameters for the maximum distance and the maximum
     * 	angle.
     * 
     * 	The curve is approximated by a series of line segments in which
     * 	1) the distance in object space between a line segment and the
     * 	actual curve must be less than the maxdist parameter and 2) the
     * 	angle in degrees between tangent vectors at the ends of a line
     * 	segment must be less than the maxangle parameter.
     * 
     * 	maxdist is the distance in real space between a line segment
     * 	and the actual curve.
     * 
     * 	maxangle is the angle (in degrees) between tangent vectors at
     * 	the ends of a line segment.
     * 
     * 	The smaller the values for maxdist and maxangle, the finer the
     * 	resolution.
     * 
     *     NOTE: Approximation information for trimming, hole, and special
     *     curves is stored in the corresponding surface. The ctech statement
     *     for the surface is used, not the ctech statement applied to the
     *     curv2 statement. Although untrimmed surfaces have no explicit
     *     trimming loop, a loop is constructed which bounds the legal
     *     parameter range. This implicit loop follows the same rules as any
     *     other loop and is approximated according to the ctech information
     *     for the surface.
     * 
     */
    constexpr const char* OBJ_CURVE_APPROXIMATION_TECHNIQUE_TOKEN = "ctech";

    /**
     * 
     * stech  technique  resolution
     * 
     *     Free-form geometry statement.
     * 
     *     Specifies a surface approximation technique. The arguments specify
     *     the technique and resolution for the surface.
     * 
     *     You must select from one of the following techniques:
     * 
     *     stech cparma ures vres
     * 
     * 	Specifies a surface with constant parametric subdivision using
     * 	separate resolution parameters for the u and v directions. Each
     * 	patch of the surface is subdivided n times in parameter space,
     * 	where n is the resolution parameter multiplied by the degree of
     * 	the surface.
     * 
     * 	ures is the resolution parameter for the u direction.
     * 
     * 	vres is the resolution parameter for the v direction.
     * 
     * 	The larger the values for ures and vres, the finer the
     * 	resolution.  If you enter a value of 0 for both ures and vres,
     * 	each patch is approximated by two triangles.
     * 
     *     stech cparmb uvres
     * 
     * 	Specifies a surface with constant parametric subdivision, with
     * 	refinement using one resolution parameter for both the u and v
     * 	directions.
     * 
     * 	An initial triangulation is performed using only the points on
     * 	the trimming curves. This triangulation is then refined until
     * 	all edges are of an appropriate length. The resulting triangles
     * 	are not oriented along isoparametric lines as they are in the
     * 	cparma technique.
     * 
     * 	uvres is the resolution parameter for both the u and v
     * 	directions.  The larger the value, the finer the resolution.
     * 
     *     stech cspace maxlength
     * 
     * 	Specifies a surface with constant spatial subdivision.
     * 
     * 	The surface is subdivided in rectangular regions until the
     * 	length in real space of any rectangle edge is less than the
     * 	maxlength.  These rectangular regions are then triangulated.
     * 
     * 	maxlength is the length in real space of any rectangle edge.
     * 	The smaller the value, the finer the resolution.
     * 
     *     stech curv maxdist maxangle
     * 
     * 	Specifies a surface with curvature-dependent subdivision using
     * 	separate resolution parameters for the maximum distance and the
     * 	maximum angle.
     * 
     * 	The surface is subdivided in rectangular regions until 1) the
     * 	distance in real space between the approximating rectangle and
     * 	the actual surface is less than the maxdist (approximately) and
     * 	2) the angle in degrees between surface normals at the corners
     * 	of the rectangle is less than the maxangle. Following
     * 	subdivision, the regions are triangulated.
     * 
     * 	maxdist is the distance in real space between the approximating
     * 	rectangle and the actual surface.
     * 
     * 	maxangle is the angle in degrees between surface normals at the
     * 	corners of the rectangle.
     * 
     * 	The smaller the values for maxdist and maxangle, the finer the
     * 	resolution.
     * 
     */
    constexpr const char* OBJ_SURFACE_APPROXIMATION_TECHNIQUE_TOKEN = "stech";
}
#endif