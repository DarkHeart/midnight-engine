#ifndef PNG_PROVIDER_HPP
#define PNG_PROVIDER_HPP

#include "lodepng.h"

#include "Heightmap.hpp"
#include "TextureLoader.hpp"
#include "ResourceException.hpp"
#include <vector>

namespace midnight
{
namespace io
{
namespace spi
{
    /**
     * A provider for PNG file format
     * 
     */
    class PNGTextureProvider : public TextureProvider
    {
        virtual bool isLoadableExtension(const std::string& extension) const noexcept
        {
            return extension == ".png";
        }
        
        virtual midnight::Texture loadTexture(const std::string& file)
        {
            std::vector<unsigned char> image;
            unsigned width, height;

            unsigned code = lodepng::decode(image, width, height, file);
            if(code)
            {
                throw midnight::ResourceException(lodepng_error_text(code));
            }
            return midnight::Texture(width, height, image);
        }
        
        virtual Heightmap loadHeightmap(const std::string& file)
        {
            std::vector<unsigned char> image;
            unsigned width, height;

            unsigned code = lodepng::decode(image, width, height, file);
            if(code)
            {
                throw midnight::ResourceException(lodepng_error_text(code));
            }
            return Heightmap(width, height, std::move(image));
        }
    };
}
}
}
void registerPlugins();

#endif